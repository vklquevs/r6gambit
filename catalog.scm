;implementation of scheme catalogs
; assumes srfi 1,99 
;creates catalog->libraries name&version library->versions serialize-library

;types
(define $library (make-rtd 'library '#((immutable name) (immutable versions) (immutable properties))))
(define $version (make-rtd 'version '#((immutable number) (immutable requires) (immutable uri) (immutable properties))))
(define make-library (rtd-constructor $library))
(define make-version (rtd-constructor $version))
(define library? (rtd-predicate $library))
(define version? (rtd-predicate $version))
(define library-name (rtd-accessor $library 'name))
(define library-versions (rtd-accessor $library 'versions))
(define library-properties (rtd-accessor $library 'properties))
(define version-number (rtd-accessor $version 'number))
(define version-requires (rtd-accessor $version 'requires))
(define version-uri (rtd-accessor $version 'uri))
(define version-properties (rtd-accessor $version 'properties))

(define (version-property version name . default)
   (let ((default (if (null? default) #f (car default)))
	 (result (assoc name (version-properties version))))
	   (cond
	      ((and result (null? (cddr result))) (cadr result))
	      (result (cdr result))
	      (else default))))   

 
(define (name&version name)
 (cond
  ((and (symbol? (car name)) (pair? (cdr name)))
   (call-with-values 
    (lambda () (name&version (cdr name)))
    (lambda (n v) (values (cons (car name) n) v))))
  ((and (symbol? (car name)) (null? (cdr name))) (values name '()))
  ((pair? (car name)) (values '() (car name)))))
	  
;parser

	(define catalog->libraries
	 (let ()
	  (define (find proc lst default)
	   (call/cc (lambda (return)
		     (for-each (lambda (f) (if (proc f) (return f))) lst)
		     (return default))))
	  (define (element-ref name lst) 
	   (let ((result (find (lambda (f) (and (pair? f) (eq? (car f) name))) lst '())))
	    (if (pair? result) (cdr result) result)))

	  (define (form->version form base)
	   (cond
	    ((string? form) (form->version (list '() form)))
	    (else
	     (let ((properties (element-ref '@ (cdr form))) 
		   (requires (element-ref 'requires (cdr form)))
		   (version (car form))
		   (path (find string? (cdr form) #f)))
	      (make-version version requires (if path (resolve-uri base (uri path)) "") properties) 
	     ))))

	  (define (read-catalog path)
		(cond
		    ((not (uri? path)) (r6rs#assertion-violation 'read-catalog "path must be a uri"))
		    ((or (not (uri-scheme path)) (not (eq? 'file (uri-scheme path)))) (r6rs#assertion-violation 'read-catalog "read-catalog only supports the file scheme" path))
		    (else (with-exception-catcher (lambda (e) '())
		   		(lambda () (with-input-from-file (uri-part path 'path) read)))))) 


	(define (main base-uri body)
	 (define (form->catalog form)
	  (if (string? (cadr form)) 
	   (catalog->libraries (cadr form) 
		(cons (read-catalog (cadr form)) (cddr form))) 
	   (catalog->libraries base-uri (cdr form))))
	 (define (form->library form) 
	  (let ((name (cadr form))
		(versions (element-ref 'versions (cddr form)))
		(properties (element-ref '@ (cddr form)))
		(base (find string? (cddr form) #f)))
	   (make-library name (map (lambda (f) (form->version f (if base (uri base) base-uri))) versions) properties)))
	 (let ((result '()))
	  ;generate libraries and flatten any internal lists
	  (for-each (lambda (x) 
		     (cond 
		      ((pair? x) (set! result (append x result)))
		      ((null? x) #f)
		      (else (set! result (cons x result)))))
	   (if (pair? body)
		(map (lambda (f)
		 (case (car f)
		  ((catalog) (form->catalog f))
		  ((library) (form->library f))
		  ((@) '())
		  (else '())))
	    		(cdr body))
		'()))
	  (reverse result)))

	(lambda (base-uri . body)
	  (cond
	      ((not (uri? base-uri)) (r6rs#assertion-violation 'catalog->libraries "first argument must be a uri"))
	      ((null? body) (main base-uri (read-catalog base-uri)))
	      (else (main base-uri (car body)))))))			


(define (serialize-library l . base)
#|
	(define (last-index-of s c)
	    (let loop ((i (string-length s)))
		(if (< i 0) #f
		(if (char=? c (string-ref s i)) i (loop (- i 1)))))) 
	(define (relativize path base)
		(let ((l1 (string-length path))
		      (l2 (string-length base)))
		(cond 
		   ((zero? l2) path)
		   ((char=? (string-ref path 0) "/") path)
		   (else (string-append (substring base 0 (last-index-of base #\/)) path)))))
|#
	(define (serialize-version v)
		`(,(version-number v) 
		  ,@(if (null? (version-requires v)) 
			'() 
			`((requires ,@(version-requires v))))
		  ,(if (null? base) (uri->string (version-uri v)) (relative-uri (car base) (version-uri v)))
		  ,@(if (null? (version-properties v))
			'()
			`((@ ,@(version-properties v))))))
    `(library 
	,(library-name l) 
	,@base
	,@(if (null? (library-properties l)) '() `((@ ,@(library-properties l))))
	(versions ,@(map serialize-version (library-versions l)))))

(define (satisfies-requirements? requirement features)
      (define (satisfied? requirement)
	(cond 
	  ((null? requirement) #t)
	  ((pair? requirement)
	    (case (car requirement)
	      ((and)
	       (all-satisfied? (cdr requirement)))
	      ((or)
	       (any-satisfied? (cdr requirement)))
	      ((not)
	       (not (satisfied? (cadr requirement))))
		;otherwise it is the same as and
	      (else (all-satisfied? requirement))))
		;we assume that requirement is a single symbol
	    (else (memq requirement features))))
      
      ; True if every requirement in LIST is satisfied.

      (define (all-satisfied? list)
	(if (null? list)
	    #t
	    (and (satisfied? (car list))
		 (all-satisfied? (cdr list)))))
      
      ; True if any requirement in LIST is satisfied.

      (define (any-satisfied? list)
	(if (null? list)
	    #f
	    (or (satisfied? (car list))
		(any-satisfied? (cdr list)))))

	
	(satisfied? requirement))

;returns the most specific version based on name version-reference and features
	(define (find-version catalog name version-reference features)
	 (define (find-library catalog name)
	  (hash-table-ref/default catalog name #f))
	 (let ((library (find-library catalog name)))
	  (if library
	   ;return the first version that satisfies the version reference and requires clause
	   (car (filter (lambda (v) 
			(and (version-reference v) (satisfies-requirements? (version-requires v) features))) (library-versions library)))
		#f)))
		
; given a uri or a version, returns the library form. 
(define (read-library version-or-uri)
 (cond
    ((version? version-or-uri) (read-library (version-uri version-or-uri)))
    ((string? version-or-uri) (read-library (uri version-or-uri)))
    ((uri? version-or-uri)
     (let* ((fragment (uri-fragment version-or-uri))
	    (path (uri-part version-or-uri 'path))
	    (reader (if fragment 
		     (lambda () 
		      (let loop ((i (string->number fragment))) 
		       (if (= i 0) (read) (begin (read) (loop (- i 1)))))) 
		     read)))

      (with-input-from-file path reader)))
    (else (r6rs#assertion-violation 'read-library "argument must be a version or a uri" version-or-uri))))

;validate a catalog. Checks that all the files are reachable and that the name and version of the library matches what is registered in the catalog.
(define (validate-catalog base-dir catalog)
   (define (check-library library)
	(for-each (lambda (v)
	    (cond
 		((or (not (uri-scheme (version-uri v))) (not (eq? 'file (uri-scheme (version-uri v))))) 
			(r6rs#assertion-violation 'ex:validate-catalog "We only support file based uris" library v))
		((not (file-exists? (uri-part (version-uri v) 'path))) 
			(r6rs#assertion-violation 'ex:validate-catalog "file not found!" library v))
		(else (call-with-values (lambda () (name&version (cadr (read-library v))))
			(lambda (name version)
			    (cond
				((not (equal? (library-name library) name))
					(r6rs#assertion-violation 'validate-catalog "library name in catalog is not the same as library name in version!" name library))
				((not (equal? (version-number v) version))
					(r6rs#assertion-violation 'validate-catalog "library version in catalog does not have the same number as the library form" version v))))))
			))
	   (library-versions library)))
   
   (let ((libraries (catalog->libraries base-dir catalog)))
	(for-each check-library libraries)
        catalog))	
