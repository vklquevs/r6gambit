;;;===============================================================================
;;;
;;; Gambit compatibility file:
;;;
;;; Uncomment the appropriate LOAD command in macros-core.scm
;;;
;;; Most written by Arthur T Smyles. Derived from compat-r5rs.scm
;;;===============================================================================

;; A numeric string that uniquely identifies this run in the universe

(define (r6rs#unique-token)
  (number->string (inexact->exact (floor (time->seconds (current-time))))))
  

;; The letrec black hole and corresponding setter.

(define (r6rs#undefined . ignore) #!void)
(define r6rs#undefined-set! 'set!)

;; Single-character symbol prefixes.
;; No builtins may start with one of these.
;; If they do, select different values here.

(define r6rs#guid-prefix "r6rs-env##")
(define r6rs#free-prefix "~")


