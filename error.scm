;;;===============================================================================
;;;
;;; basic error system used by expander 
;;;
;;; by Arthur T Smyles.
;;;===============================================================================


;basic conditions system used to implement assertion-violation

;used to create your own simple conditions
(define r6rs#&condition (make-rtd '&condition '#() #f 'uid '0bf9b656-b071-404a-a514-0fb9d05cf518)); use same id as exceptions
(define r6rs#&compound (make-rtd '&compound '#((immutable conditions)) r6rs#&condition))
(define r6rs#&serious (make-rtd '&serious '#() r6rs#&condition))
(define r6rs#&error (make-rtd '&error '#() r6rs#&serious))
(define r6rs#&violation (make-rtd '&violation '#() r6rs#&serious))
(define r6rs#&assertion (make-rtd '&assertion '#() r6rs#&violation))
(define r6rs#&who (make-rtd '&who '#((immutable who)) r6rs#&condition))
(define r6rs#&message (make-rtd '&message '#((immutable message)) r6rs#&condition))
(define r6rs#&irritants (make-rtd '&irritants '#((immutable irritants)) r6rs#&condition))

;used in expander.scm
(define r6rs#&syntax (make-rtd '&syntax '#((immutable form) (immutable subform)) r6rs#&violation))
(define r6rs#&trace (make-rtd '&trace '#((immutable trace)) r6rs#&condition))
    
;need to define raise for your implementation

(define r6rs#condition
  (let ((constructor (rtd-constructor r6rs#&compound)))
    (lambda conditions
	(constructor (apply append (map r6rs#simple-conditions conditions)))))) 

(define r6rs#simple-conditions
  (let ((compound? (rtd-predicate r6rs#&compound))
	(conditions (rtd-accessor r6rs#&compound 'conditions)))
    (lambda (condition)
      (cond
	((compound? condition) (conditions condition))
	(else (list condition))))))

(define r6rs#make-who-condition (rtd-constructor r6rs#&who))
(define r6rs#make-message-condition (rtd-constructor r6rs#&message))
(define r6rs#make-irritants-condition (rtd-constructor r6rs#&irritants))
(define r6rs#make-syntax-violation (rtd-constructor r6rs#&syntax))
(define r6rs#make-assertion-violation (rtd-constructor r6rs#&assertion))
(define r6rs#make-error (rtd-constructor r6rs#&error))

(define (r6rs#assertion-violation who message . irritants)
  (raise 
    (if who
    (r6rs#condition (r6rs#make-who-condition who) 
	       (r6rs#make-message-condition message) 
	       (r6rs#make-irritants-condition irritants)
	       (r6rs#make-assertion-violation))
    (r6rs#condition (r6rs#make-message-condition message) 
	       (r6rs#make-irritants-condition irritants)
	       (r6rs#make-assertion-violation)))))

;to be used by native code to throw errors compatible with r6rs
(define (r6rs#error-object who message . irritants)
    (if who
    (r6rs#condition (r6rs#make-who-condition who) 
	       (r6rs#make-message-condition message) 
	       (r6rs#make-irritants-condition irritants)
	       (r6rs#make-error))
    (r6rs#condition (r6rs#make-message-condition message) 
	       (r6rs#make-irritants-condition irritants)
	       (r6rs#make-error))))

(define (r6rs#error who message . irritants)
	(raise (apply r6rs#error-object who message irritants)))
 
