;This is a superset of srfi-66
(library (core bytevectors)
  (export u8vector? make-u8vector u8vector u8vector->list list->u8vector u8vector-length u8vector-ref u8vector-set! u8vector=? u8vector-compare u8vector-copy! u8vector-copy native-endianness u8vector-fill!)
  (import (primitives u8vector? make-u8vector u8vector u8vector->list list->u8vector u8vector-length u8vector-ref u8vector-set! u8vector=? u8vector-compare u8vector-copy! u8vector-copy native-endianness u8vector-fill!)))

