;exports srfi-1-min
(library (core lists)
    (export find-tail filter fold every)
    (import (primitives find-tail filter fold every)))
