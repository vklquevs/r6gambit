;; Nonstandard R5RS library:

(library (r5rs)  
  (export 
   
   ;; core primitives
   
   set!
   
   ;; rnrs base
   
   begin if lambda quote and or
   define define-syntax let-syntax letrec-syntax
   ...
   
   let let* letrec
   case cond else =>
   quasiquote unquote unquote-splicing
   syntax-rules 
   
   * + - / < <= = > >= abs acos append apply asin atan angle 
   boolean? call-with-current-continuation 
   call-with-values car cdr caar cadr cdar cddr
   caaar caadr cadar caddr cdaar cdadr cddar cdddr caaaar caaadr caadar caaddr cadaar
   cadadr caddar cadddr cdaaar cdaadr cdadar cdaddr cddaar cddadr cdddar cddddr
   ceiling char? char->integer char=? char<? char>? char<=? char>=?
   complex? cons cos 
   denominator dynamic-wind 
   eq? equal? eqv? even? exact? exp expt floor for-each
   gcd imag-part inexact? integer->char integer?
   lcm length list list->string
   list->vector list-ref list-tail list? log magnitude make-polar
   make-rectangular make-string make-vector map max min
   negative? not null? number->string number? numerator
   odd? pair? 
   positive? procedure? rational? rationalize
   real-part real? reverse round
   sin sqrt string string->list string->number string->symbol
   string-append 
   string-copy string-length string-ref string<=? string<?
   string=? string>=? string>? string? substring symbol->string symbol? tan
   truncate values vector vector->list
   vector-fill! vector-length vector-ref vector-set! vector? zero?
   
   ;; rnrs eval
   
   eval
   
   ;; rnrs load
   
   ;load
   
   ;; rnrs control
   
   do
   ;; rnrs io ports
   input-port? output-port?

   ;; rnrs io simple
   
   call-with-input-file call-with-output-file 
   close-input-port close-output-port current-input-port current-output-port
   display eof-object? newline open-input-file open-output-file peek-char
   read read-char with-input-from-file with-output-to-file write write-char
   
   ;; rnrs unicode
   
   char-upcase char-downcase char-ci=? char-ci<? char-ci>?
   char-ci<=? char-ci>=? char-alphabetic? char-numeric? char-whitespace?
   char-upper-case? char-lower-case? string-ci=? string-ci<? string-ci>?
   string-ci<=? string-ci>=?
   
   ;; rnrs mutable pairs
   
   set-car! set-cdr!
   
   ;; rnrs lists
   
   assoc assv assq member memv memq
   
   ;; rnrs mutable-strings
   
   string-set! string-fill!
   
   ;; rnrs r5rs
   
   null-environment scheme-report-environment delay force
   exact->inexact inexact->exact quotient remainder modulo)
  
  ;; Not necessary to use only and except here, but keep
  ;; them because they contain useful information.
  
  (import (for (except (core primitives) _ ...) expand run)
          (core let)
	  (core derived)
	  (core quasiquote)
	  (for (core with-syntax) expand)
	  (for (core syntax-rules)	expand)
          (for (only (core primitives) _ ... set!) expand)
	  (for (primitives map) expand run)
	  (primitives 
           
           ;; R5RS primitives:
           
           * + - / < <= = > >= abs acos append apply asin atan angle 
           boolean? call-with-current-continuation 
           call-with-values car cdr caar cadr cdar cddr
           caaar caadr cadar caddr cdaar cdadr cddar cdddr caaaar caaadr caadar caaddr cadaar
           cadadr caddar cadddr cdaaar cdaadr cdadar cdaddr cddaar cddadr cdddar cddddr
           ceiling char? char->integer char=? char<? char>? char<=? char>=?
           complex? cons cos 
           denominator dynamic-wind 
           eq? equal? eqv? even? exact? exp expt floor for-each
           gcd imag-part inexact? integer->char integer?
           lcm length list list->string
           list->vector list-ref list-tail list? log magnitude make-polar
           make-rectangular make-string make-vector 
	   max min negative? not null? number->string number? numerator
           odd? pair? 
           positive? procedure? rational? rationalize
           real-part real? reverse round
           sin sqrt string string->list string->number string->symbol
           string-append 
           string-copy string-length string-ref string<=? string<?
           string=? string>=? string>? string? substring symbol->string symbol? tan
           truncate values vector vector->list
           vector-fill! vector-length vector-ref vector-set! vector? zero?

	   ;error messages (used internally only)
	   r6rs#assertion-violation	   
	   ;io
	    input-port? output-port?
            call-with-input-file call-with-output-file 
            close-input-port close-output-port current-input-port current-output-port
            display eof-object? newline open-input-file open-output-file peek-char
            read read-char with-input-from-file with-output-to-file write write-char
	;characters
            char-upcase char-downcase char-ci=? char-ci<? char-ci>?
            char-ci<=? char-ci>=? char-alphabetic? char-numeric? char-whitespace?
            char-upper-case? char-lower-case? string-ci=? string-ci<? string-ci>?
            string-ci<=? string-ci>=?
        ;mutable pairs
          set-car! set-cdr!
	;other lists
          assoc assv assq member memv memq
          string-set! string-fill!
          
	;misc
         exact->inexact inexact->exact quotient remainder modulo

  ))

(define (scheme-report-environment n)
    (if (= n 5) (environment '(r5rs))
      (r6rs#assertion-violation 'scheme-report-environment "Argument should be 5" n))
    )

 (define null-environment
    (let ((null-env #f))
      (lambda (n)
	(if null-env #f (set! null-env (environment '(only (r5rs)
                           begin if lambda quote set! and or
                           define define-syntax let-syntax letrec-syntax 
                           let let* letrec
                           case cond else =>
                           quasiquote unquote unquote-splicing
                           syntax-rules ... do)
                        )))
        (if (= n 5) null-env
          (r6rs#assertion-violation 'null-environment "Argument should be 5" n))
        )))
  
(define force
    (lambda (object)
      (object)))
  
(define-syntax delay
    (syntax-rules ()
      ((delay expression)
       (make-promise (lambda () expression)))))

 (define make-promise
    (lambda (proc)
      (let ((result-ready? #f)
            (result #f))
        (lambda ()
          (if result-ready?
              result
              (let ((x (proc)))
                (if result-ready?
                    result
                    (begin (set! result-ready? #t)
                           (set! result x)
                           result))))))))
  (define-syntax do
    (lambda (orig-x)
      (syntax-case orig-x ()
        ((_ ((var init . step) ...) (e0 e1 ...) c ...)
         (with-syntax (((step ...)
                        (map (lambda (v s)
                               (syntax-case s ()
                                 (()  v)
                                 ((e) (syntax e))
                                 (_   (syntax-violation 'do "Invalid step" orig-x s))))
                             (syntax (var ...))
                             (syntax (step ...)))))
           (syntax-case (syntax (e1 ...)) ()
             (()          (syntax (let do ((var init) ...)
                                    (if (not e0)
                                        (begin c ... (do step ...))))))
             ((e1 e2 ...) (syntax (let do ((var init) ...)
                                    (if e0
                                        (begin e1 e2 ...)
                                        (begin c ... (do step ...))))))))))))                     
)
