;core records picks up the err5rs record procedures and inspection api
(library (core records procedural)
	 (export make-rtd rtd? rtd-constructor rtd-predicate rtd-destructor rtd-accessor rtd-mutator record->vector)
	 (import (primitives make-rtd rtd? rtd-constructor rtd-predicate rtd-destructor rtd-accessor rtd-mutator record->vector)))

(library (core records inspection)
	 (export record? record-rtd rtd-name rtd-parent rtd-field-names rtd-all-field-names rtd-uid rtd-sealed? rtd-opaque? rtd-generative? ; record inspection 
		 rtd-field-mutable? rtd-field-printable? rtd-field-extensional? rtd-field-init? ; field properties
		 rtd-fold-left rtd-fold-right rtd-fold-all-left rtd-fold-all-right rtd-map rtd-for-each rtd-map-all rtd-for-all) ; field manipulation
	 (import (primitives record? record-rtd rtd-name rtd-parent rtd-field-names rtd-all-field-names rtd-uid rtd-sealed? rtd-opaque? rtd-generative? ; record inspection 
		 rtd-field-mutable? rtd-field-printable? rtd-field-extensional? rtd-field-init? ; field properties
		 rtd-fold-left rtd-fold-right rtd-fold-all-left rtd-fold-all-right rtd-map rtd-for-each rtd-map-all rtd-for-all)
	 ))

  
