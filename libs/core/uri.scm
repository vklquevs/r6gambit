(library (core uri)
   (export $uri uri make-uri uri? uri-scheme uri-body uri-query uri-fragment uri->string resolve-uri relative-uri uri-part register-uri-part! uri-part?)
   (import (primitives $uri uri make-uri uri? uri-scheme uri-body uri-query uri-fragment uri->string resolve-uri relative-uri uri-part register-uri-part! uri-part?)))
