(library (gambit bytevectors) 
	 
(export
  ;s8
s8vector s8vector->list s8vector-append s8vector-copy s8vector-fill! list->s8vector s8vector-length s8vector-ref s8vector-set! make-s8vector s8vector? subs8vector
  ;u8
u8vector u8vector->list u8vector-append u8vector-copy u8vector-fill! make-u8vector u8vector-length u8vector-ref u8vector-set! list->u8vector u8vector? subu8vector
;u16
u16vector u16vector->list u16vector-append u16vector-copy u16vector-fill! list->u16vector u16vector-length u16vector-ref u16vector-set! make-u16vector subu16vector u16vector?
  ;s16
s16vector s16vector->list s16vector-append s16vector-copy s16vector-fill! make-s16vector s16vector-length s16vector-ref s16vector-set! subs16vector s16vector? list->s16vector
  ;u32
u32vector u32vector->list u32vector-append u32vector-copy u32vector-fill! list->u32vector u32vector-length u32vector-ref u32vector-set! make-u32vector u32vector? subu32vector
  ;s32
s32vector s32vector->list s32vector-append s32vector-copy s32vector-fill! list->s32vector s32vector-length s32vector-ref s32vector-set! make-s32vector s32vector? subs32vector
  ;s64
s64vector s64vector->list s64vector-append s64vector-copy s64vector-fill! list->s64vector s64vector-length s64vector-ref s64vector-set! make-s64vector s64vector? subs64vector
  ;u64
u64vector u64vector->list u64vector-append u64vector-copy u64vector-fill! list->u64vector u64vector-length u64vector-ref u64vector-set! make-u64vector u64vector? subu64vector
  ;f32
f32vector f32vector->list f32vector-append f32vector-copy f32vector-fill!  f32vector-length f32vector-ref f32vector-set!  f32vector? subf32vector list->f32vector make-f32vector
  ;f64
f64vector f64vector->list f64vector-append f64vector-copy f64vector-fill! subf64vector f64vector-length f64vector-ref f64vector-set!  f64vector?  list->f64vector make-f64vector)
(import (primitives
  ;s8
s8vector s8vector->list s8vector-append s8vector-copy s8vector-fill! list->s8vector s8vector-length s8vector-ref s8vector-set! make-s8vector s8vector? subs8vector
  ;u8
u8vector u8vector->list u8vector-append u8vector-copy u8vector-fill! make-u8vector u8vector-length u8vector-ref u8vector-set! list->u8vector u8vector? subu8vector
;u16
u16vector u16vector->list u16vector-append u16vector-copy u16vector-fill! list->u16vector u16vector-length u16vector-ref u16vector-set! make-u16vector subu16vector u16vector?
  ;s16
s16vector s16vector->list s16vector-append s16vector-copy s16vector-fill! make-s16vector s16vector-length s16vector-ref s16vector-set! subs16vector s16vector? list->s16vector
  ;u32
u32vector u32vector->list u32vector-append u32vector-copy u32vector-fill! list->u32vector u32vector-length u32vector-ref u32vector-set! make-u32vector u32vector? subu32vector
  ;s32
s32vector s32vector->list s32vector-append s32vector-copy s32vector-fill! list->s32vector s32vector-length s32vector-ref s32vector-set! make-s32vector s32vector? subs32vector
  ;s64
s64vector s64vector->list s64vector-append s64vector-copy s64vector-fill! list->s64vector s64vector-length s64vector-ref s64vector-set! make-s64vector s64vector? subs64vector
  ;u64
u64vector u64vector->list u64vector-append u64vector-copy u64vector-fill! list->u64vector u64vector-length u64vector-ref u64vector-set! make-u64vector u64vector? subu64vector
  ;f32
f32vector f32vector->list f32vector-append f32vector-copy f32vector-fill!  f32vector-length f32vector-ref f32vector-set!  f32vector? subf32vector list->f32vector make-f32vector
  ;f64
f64vector f64vector->list f64vector-append f64vector-copy f64vector-fill! subf64vector f64vector-length f64vector-ref f64vector-set!  f64vector?  list->f64vector make-f64vector
)))
