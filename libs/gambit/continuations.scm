(library (gambit continuations)
	(export continuation?  continuation-capture continuation-graft continuation-return display-continuation-environment display-continuation-dynamic-environment display-continuation-backtrace)
        (import (primitives continuation?  continuation-capture continuation-graft continuation-return display-continuation-environment display-continuation-dynamic-environment display-continuation-backtrace)))

