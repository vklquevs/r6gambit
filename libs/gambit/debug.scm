(library (gambit debug)
	 (export trace untrace step step-level-set! break unbreak generate-proper-tail-calls display-environment-set! pretty-print pp gc-report-set!)
	 (import (primitives trace untrace step step-level-set! break unbreak generate-proper-tail-calls display-environment-set! pretty-print pp gc-report-set!)))
