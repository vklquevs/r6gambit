(library (gambit exceptions)
	 (export current-exception-handler with-exception-handler with-exception-catcher raise abort display-exception display-exception-in-context)
	 (import (primitives current-exception-handler with-exception-handler with-exception-catcher raise abort display-exception display-exception-in-context)))
