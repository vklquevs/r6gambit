(library (gambit extensions)
	 (export 
	   ;boxes
	   box box? unbox set-box!
	   ;keywords
	   keyword? keyword->string string->keyword make-uninterned-keyword uninterned-keyword?
	   ;symbols
	   gensym make-uninterned-symbol uninterned-symbol?
	   ;serialization
	   object->u8vector u8vector->object
	   ;other
	   void 
	   ;special forms
	   define-macro
	   ;currently not supported
	   ;include declare
	   )
	 (import (for (core primitives) expand run)
		 (for (only (r5rs) let apply) expand run)
	  (primitives 
	   ;boxes
	   box box? unbox set-box!
	   ;keywords
	   keyword? keyword->string string->keyword make-uninterned-keyword uninterned-keyword?
	   ;symbols
	   gensym make-uninterned-symbol uninterned-symbol?
	   ;serialization
	   object->u8vector u8vector->object
	   ;other
	   void 
	   ;special forms
	   ;currently not supported
	   ;include define-macro declare
	   ))

(define-syntax define-macro
   (lambda (x)
     (syntax-case x ()
       ((_ (name . params) body1 body2 ...)
        (syntax (define-macro name (lambda params body1 body2 ...))))
       ((_ name expander)
        (syntax (define-syntax name
                  (lambda (y)
                    (syntax-case y ()
                      ((k . args)
                       (let ((lst (syntax->datum (syntax args))))
                         (datum->syntax
                          (syntax k)
                          (apply expander lst))))))))))))

)
