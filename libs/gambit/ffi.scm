(library (gambit ffi)
	(export foreign? foreign-tags foreign-address foreign-release! foreign-released?)
	(import (gambit ffi macros)
		(primitives foreign? foreign-tags foreign-address foreign-release! foreign-released?)))
