; from gambit docs 16.1, 16.2, 16.8
(library (gambit files)
  (export 
    ;path operations
    current-directory path-expand path-normalize path-extension path-strip-extension path-directory path-strip-directory path-strip-trailing-directory-separator path-volume path-strip-volume 
    ;filesystem operations
    create-directory create-fifo create-link create-symbolic-link rename-file copy-file delete-file delete-directory directory-files file-exists?
    ;file-info
    file-info file-info? file-info-type file-info-device file-info-inode file-info-mode file-info-number-of-links file-info-owner file-info-group file-info-size file-info-last-access-time file-info-last-modification-time file-info-last-change-time file-info-creation-time
    ;combines file-info with the cooresponding accessor
    file-type file-device file-inode file-mode file-number-of-links file-owner file-group file-size file-last-access-time path file-last-modification-time file-last-change-time file-attributes file-creation-time)
  (import (primitives 
    ;path operations
    current-directory path-expand path-normalize path-extension path-strip-extension path-directory path-strip-directory path-strip-trailing-directory-separator path-volume path-strip-volume 
    ;filesystem operations
    create-directory create-fifo create-link create-symbolic-link rename-file copy-file delete-file delete-directory directory-files file-exists?
    ;file-info
    file-info file-info? file-info-type file-info-device file-info-inode file-info-mode file-info-number-of-links file-info-owner file-info-group file-info-size file-info-last-access-time file-info-last-modification-time file-info-last-change-time file-info-creation-time
    ;combines file-info with the cooresponding accessor
    file-type file-device file-inode file-mode file-number-of-links file-owner file-group file-size file-last-access-time path file-last-modification-time file-last-change-time file-attributes file-creation-time)))
