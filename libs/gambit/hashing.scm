(library (gambit hashing)
  (export object->serial-number serial-number->object unbound-serial-number-exception? unbound-serial-number-exception-procedure unbound-serial-number-exception-arguments symbol-hash keyword-hash string=?-hash string-ci=?-hash eq?-hash eqv?-hash equal?-hash)
  (import (primitives object->serial-number serial-number->object unbound-serial-number-exception? unbound-serial-number-exception-procedure unbound-serial-number-exception-arguments symbol-hash keyword-hash string=?-hash string-ci=?-hash eq?-hash eqv?-hash equal?-hash)))
