(library (gambit io)
	 (export
	   ;object ports
	   input-port? output-port? port? read read-all write newline force-output close-input-port close-output-port close-port input-port-timeout-set! output-port-timeout-set!
	   ;character ports
	   input-port-line inport-port-column output-port-line output-port-column output-port-width read-char peek-char write-char read-line read-substring write-substring input-port-readtable output-port-readtable input-port-readtable-set! output-port-readtable-set!
	   ;byte ports
	   read-u8 write-u8 read-subu8vector write-subu8vector
	   ;device ports
	   ;filesystem devices
	   open-file open-input-file open-output-file call-with-input-file call-with-output-file with-input-from-file with-output-to-file input-port-byte-position output-port-byte-position
	   ;processes
	   open-process open-input-process open-output-process call-with-input-process call-with-output-process with-input-from-process with-output-to-process 
	   ;network devices
	   open-tcp-client open-tcp-server 
	   ;directory devices
	   open-directory
	   ;vector device
	   open-vector open-input-vector open-output-vector call-with-input-vector call-with-output-vector with-input-from-vector with-output-to-vector open-vector-pipe get-output-vector
	   ;string device
	   open-string open-input-string open-output-string call-with-input-string call-with-output-string with-input-from-string with-output-to-string open-string-pipe get-output-string object->string
	   ;u8vector device
	   open-u8vector open-input-u8vector open-output-u8vector call-with-input-u8vector call-with-output-u8vector with-input-from-u8vector with-output-to-u8vector open-u8vector-pipe get-output-u8vector
	   ;parameter object accessors
	   current-input-port current-output-port current-error-port
	   ;r5rs
	   display eof-object?
	   )
	 (import (primitives
		   ;object ports
		   input-port? output-port? port? read read-all write newline force-output close-input-port close-output-port close-port input-port-timeout-set! output-port-timeout-set!
		   ;character ports
		   input-port-line inport-port-column output-port-line output-port-column output-port-width read-char peek-char write-char read-line read-substring write-substring input-port-readtable output-port-readtable input-port-readtable-set! output-port-readtable-set!
		   ;byte ports
		   read-u8 write-u8 read-subu8vector write-subu8vector
		   ;device ports
		   ;filesystem devices
		   open-file open-input-file open-output-file call-with-input-file call-with-output-file with-input-from-file with-output-to-file input-port-byte-position output-port-byte-position
		   ;processes
		   open-process  open-input-process open-output-process call-with-input-process call-with-output-process with-input-from-process with-output-to-process 
		   ;network devices
		   open-tcp-client open-tcp-server 
		   ;directory devices
		   open-directory
		   ;vector device
		   open-vector open-input-vector open-output-vector call-with-input-vector call-with-output-vector with-input-from-vector with-output-to-vector open-vector-pipe get-output-vector
		   ;string device
		   open-string open-input-string open-output-string call-with-input-string call-with-output-string with-input-from-string with-output-to-string open-string-pipe get-output-string object->string
		   ;u8vector device
		   open-u8vector open-input-u8vector open-output-u8vector call-with-input-u8vector call-with-output-u8vector with-input-from-u8vector with-output-to-u8vector open-u8vector-pipe get-output-u8vector
		   ;parameter object accessors
		   current-input-port current-output-port current-error-port
		   ;r5rs
		   display eof-object?
		   ))
)
