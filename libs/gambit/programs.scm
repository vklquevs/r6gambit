;from gambit manual 16.3, 16.4, 16.5, 16.6
(library (gambit programs)
	 (export shell-command exit command-line get-env set-env)
	 (import (primitives shell-command exit command-line get-env set-env)))
