(library (gambit records)
     (export define-structure)
     (import (for (core records procedural) expand run)
	     (for (r5rs) expand run) 
	     (for (core primitives) expand run)
	     (for (core with-syntax) expand run))

   (define-syntax define-structure
      (lambda (x)
	(define (c-name s t) (datum->syntax t (string->symbol (string-append "make-" (symbol->string (syntax->datum s))))))
	(define (p-name s t) (datum->syntax t (string->symbol (string-append (symbol->string (syntax->datum s)) "?"))))
        (define (f-name s fs t)
           (define structure-name (symbol->string (syntax->datum s)))
      	   (datum->syntax t (map (lambda (f)
			(let* ((f-name (symbol->string f))
			      (a-name (string-append structure-name "-" f-name))
			      (m-name (string-append a-name "-set!")))
			(list f (string->symbol a-name) (string->symbol m-name))))
				(syntax->datum fs))))
		
         (syntax-case x ()
             ((define-structure name field ...)
               (with-syntax [(constructor (c-name #'name #'define-structure))
			     (predicate (p-name #'name #'define-structure))
			     (((field accessor mutator) ...) (f-name #'name #'(field ...) #'define-structure))]
		  #'(begin
			(define name (make-rtd 'name '#(field ...)))
			(define constructor (rtd-constructor name))
			(define predicate (rtd-predicate name))
			(define accessor (rtd-accessor name 'field)) ...
			(define mutator (rtd-mutator name 'field)) ...)))))))
