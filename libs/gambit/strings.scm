(library (gambit strings)
   (export append-strings substring-fill! substring-move! string-shrink!)
   (import (primitives append-strings substring-fill! substring-move! string-shrink!)))
   
