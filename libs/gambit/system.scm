; returns info about the current gambit build from 6.4
(define library (gambit system)
    (export system-version system-version-string system-type system-type-string configure-command-string system-stamp)
    (import (primitives system-version system-version-string system-type system-type-string configure-command-string system-stamp)))
