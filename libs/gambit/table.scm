(library (gambit table)
  (export make-table table? table-length table-ref table-set! table-search table-for-each table->list list->table unbound-table-key-exception? unbound-table-key-exception-procedure unbound-table-key-exception-arguments table-copy table-merge! table-merge)
  (import (primitives make-table table? table-length table-ref table-set! table-search table-for-each table->list list->table unbound-table-key-exception? unbound-table-key-exception-procedure unbound-table-key-exception-arguments table-copy table-merge! table-merge)))
