
(library (gambit threads (4)) 
	 (export
	   ;threads
	   current-thread make-thread thread? thread-name  thread-thread-group thread-specific thread-specific-set!  thread-base-priority thread-base-priority-set! thread-priority-boost thread-priority-boost-set!  thread-quantum thread-quantum-set!  thread-init! thread-start!  thread-join! thread-yield! thread-sleep!  thread-suspend! thread-resume!   thread-terminate! thread-send   thread-receive thread-mailbox-rewind thread-mailbox-next thread-mailbox-extract-and-rewind    
	   ;threadgroups
	   make-thread-group thread-group?  thread-group-name thread-group-parent thread-group-resume!  thread-group-suspend!  thread-group-terminate!  
	   ;mutexes
	   make-mutex mutex?  mutex-name mutex-specific mutex-specific-set! mutex-state  mutex-lock! mutex-unlock!   
	   ;condition-variables
	  make-condition-variable condition-variable? condition-variable-name condition-variable-specific condition-variable-specific-set!      condition-variable-broadcast! condition-variable-signal! 
	   ;exceptions
	   deadlock-exception?  mailbox-receive-timeout-exception-arguments mailbox-receive-timeout-exception-procedure mailbox-receive-timeout-exception?  started-thread-exception-arguments started-thread-exception-procedure started-thread-exception?  terminated-thread-exception-arguments terminated-thread-exception-procedure terminated-thread-exception? uninitialized-thread-exception? uninitialized-thread-exception-procedure uninitialized-thread-exception-arguments abandoned-mutex-exception?  join-timeout-exception? join-timeout-exception-procedure join-timeout-exception-arguments uncaught-exception? uncaught-exception-procedure uncaught-exception-arguments uncaught-exception-reason
	   
)
	 (import (primitives
	   ;threads
	   current-thread make-thread thread? thread-name  thread-thread-group thread-specific thread-specific-set!  thread-base-priority thread-base-priority-set! thread-priority-boost thread-priority-boost-set!  thread-quantum thread-quantum-set!  thread-init! thread-start!  thread-join! thread-yield! thread-sleep!  thread-suspend! thread-resume!   thread-terminate! thread-send   thread-receive thread-mailbox-rewind thread-mailbox-next thread-mailbox-extract-and-rewind    
	   ;threadgroups
	   make-thread-group thread-group?  thread-group-name thread-group-parent thread-group-resume!  thread-group-suspend!  thread-group-terminate!  
	   ;mutexes
	   make-mutex mutex?  mutex-name mutex-specific mutex-specific-set! mutex-state  mutex-lock! mutex-unlock!   
	   ;condition-variables
	  make-condition-variable condition-variable? condition-variable-name condition-variable-specific condition-variable-specific-set!      condition-variable-broadcast! condition-variable-signal! 
	   ;exceptions
	   deadlock-exception?  mailbox-receive-timeout-exception-arguments mailbox-receive-timeout-exception-procedure mailbox-receive-timeout-exception?  started-thread-exception-arguments started-thread-exception-procedure started-thread-exception?  terminated-thread-exception-arguments terminated-thread-exception-procedure terminated-thread-exception? uninitialized-thread-exception? uninitialized-thread-exception-procedure uninitialized-thread-exception-arguments abandoned-mutex-exception? scheduler-exception? scheduler-exception-reason deadlock-exception? abandoned-mutex-exception? join-timeout-exception? join-timeout-exception-procedure join-timeout-exception-arguments uncaught-exception? uncaught-exception-procedure uncaught-exception-arguments uncaught-exception-reason
)))

