; from gambit manual 16.7
(library (gambit time)
	 (export time? current-time time->seconds seconds->time process-times cpu-time real-time time)
	 (import (primitives time? current-time time->seconds seconds->time process-times cpu-time real-time time)))
