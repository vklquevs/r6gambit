(library (gambit vectors)
  (export 
	;vector extensions
	vector-copy subvector vector-append append-vectors subvector-fill! subvector-move! vector-shrink!
	;;homogeneous vectors
	;signed-8
	s8vector? make-s8vector s8vector s8vector-length s8vector-ref s8vector-set! s8vector->list list->s8vector s8vector-fill! subs8vector-fill! append-s8vectors s8vector-copy s8vector-append subs8vector subs8vector-move! s8vector-shrink!
	;unsigned-8
	u8vector? make-u8vector u8vector u8vector-length u8vector-ref u8vector-set! u8vector->list list->u8vector u8vector-fill! subu8vector-fill! append-u8vectors u8vector-copy u8vector-append subu8vector subu8vector-move! u8vector-shrink!
	;signed-16
	s16vector? make-s16vector s16vector s16vector-length s16vector-ref s16vector-set! s16vector->list list->s16vector s16vector-fill! subs16vector-fill! append-s16vectors s16vector-copy s16vector-append subs16vector subs16vector-move! s16vector-shrink!
	;unsigned-16
	u16vector? make-u16vector u16vector u16vector-length u16vector-ref u16vector-set! u16vector->list list->u16vector u16vector-fill! subu16vector-fill! append-u16vectors u16vector-copy u16vector-append subu16vector subu16vector-move! u16vector-shrink!
	;signed-32
	s32vector? make-s32vector s32vector s32vector-length s32vector-ref s32vector-set! s32vector->list list->s32vector s32vector-fill! subs32vector-fill! append-s32vectors s32vector-copy s32vector-append subs32vector subs32vector-move! s32vector-shrink!
	;unsigned-32
	u32vector? make-u32vector u32vector u32vector-length u32vector-ref u32vector-set! u32vector->list list->u32vector u32vector-fill! subu32vector-fill! append-u32vectors u32vector-copy u32vector-append subu32vector subu32vector-move! u32vector-shrink!
	;signed-64
	s64vector? make-s64vector s64vector s64vector-length s64vector-ref s64vector-set! s64vector->list list->s64vector s64vector-fill! subs64vector-fill! append-s64vectors s64vector-copy s64vector-append subs64vector subs64vector-move! s64vector-shrink!
	;unsigned-64
	u64vector? make-u64vector u64vector u64vector-length u64vector-ref u64vector-set! u64vector->list list->u64vector u64vector-fill! subu64vector-fill! append-u64vectors u64vector-copy u64vector-append subu64vector subu64vector-move! u64vector-shrink!
	;float-32
	f32vector? make-f32vector f32vector f32vector-length f32vector-ref f32vector-set! f32vector->list list->f32vector f32vector-fill! subf32vector-fill! append-f32vectors f32vector-copy f32vector-append subf32vector subf32vector-move! f32vector-shrink!
	;float-64
	f64vector? make-f64vector f64vector f64vector-length f64vector-ref f64vector-set! f64vector->list list->f64vector f64vector-fill! subf64vector-fill! append-f64vectors f64vector-copy f64vector-append subf64vector subf64vector-move! f64vector-shrink!)
	(import (primitives 
	;vector extensions
	vector-copy subvector vector-append append-vectors subvector-fill! subvector-move! vector-shrink!
	;;homogeneous vectors
	;signed-8
	s8vector? make-s8vector s8vector s8vector-length s8vector-ref s8vector-set! s8vector->list list->s8vector s8vector-fill! subs8vector-fill! append-s8vectors s8vector-copy s8vector-append subs8vector subs8vector-move! s8vector-shrink!
	;unsigned-8
	u8vector? make-u8vector u8vector u8vector-length u8vector-ref u8vector-set! u8vector->list list->u8vector u8vector-fill! subu8vector-fill! append-u8vectors u8vector-copy u8vector-append subu8vector subu8vector-move! u8vector-shrink!
	;signed-16
	s16vector? make-s16vector s16vector s16vector-length s16vector-ref s16vector-set! s16vector->list list->s16vector s16vector-fill! subs16vector-fill! append-s16vectors s16vector-copy s16vector-append subs16vector subs16vector-move! s16vector-shrink!
	;unsigned-16
	u16vector? make-u16vector u16vector u16vector-length u16vector-ref u16vector-set! u16vector->list list->u16vector u16vector-fill! subu16vector-fill! append-u16vectors u16vector-copy u16vector-append subu16vector subu16vector-move! u16vector-shrink!
	;signed-32
	s32vector? make-s32vector s32vector s32vector-length s32vector-ref s32vector-set! s32vector->list list->s32vector s32vector-fill! subs32vector-fill! append-s32vectors s32vector-copy s32vector-append subs32vector subs32vector-move! s32vector-shrink!
	;unsigned-32
	u32vector? make-u32vector u32vector u32vector-length u32vector-ref u32vector-set! u32vector->list list->u32vector u32vector-fill! subu32vector-fill! append-u32vectors u32vector-copy u32vector-append subu32vector subu32vector-move! u32vector-shrink!
	;signed-64
	s64vector? make-s64vector s64vector s64vector-length s64vector-ref s64vector-set! s64vector->list list->s64vector s64vector-fill! subs64vector-fill! append-s64vectors s64vector-copy s64vector-append subs64vector subs64vector-move! s64vector-shrink!
	;unsigned-64
	u64vector? make-u64vector u64vector u64vector-length u64vector-ref u64vector-set! u64vector->list list->u64vector u64vector-fill! subu64vector-fill! append-u64vectors u64vector-copy u64vector-append subu64vector subu64vector-move! u64vector-shrink!
	;float-32
	f32vector? make-f32vector f32vector f32vector-length f32vector-ref f32vector-set! f32vector->list list->f32vector f32vector-fill! subf32vector-fill! append-f32vectors f32vector-copy f32vector-append subf32vector subf32vector-move! f32vector-shrink!
	;float-64
	f64vector? make-f64vector f64vector f64vector-length f64vector-ref f64vector-set! f64vector->list list->f64vector f64vector-fill! subf64vector-fill! append-f64vectors f64vector-copy f64vector-append subf64vector subf64vector-move! f64vector-shrink!)))


	

