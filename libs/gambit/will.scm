(library (gambit will (4)) 
	 (export make-will will? will-testator  will-execute!  )
	 (import (primitives make-will will? will-testator will-execute!)))
