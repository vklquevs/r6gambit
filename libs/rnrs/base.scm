(library (rnrs base (6))         
  
  (export 
   
   ;; Macros defined in core expander:
   
   begin if lambda quote set! and or
   define define-syntax let-syntax letrec-syntax
   _ ...
   
   ;; Derived syntax:
   
   let let* letrec letrec* let-values let*-values
   case cond else =>
   assert
   quasiquote unquote unquote-splicing
   syntax-rules 
   identifier-syntax
   
   ;; R5RS primitives:
   
   * + - / < <= = > >= abs acos append apply asin atan angle 
   boolean? call-with-current-continuation 
   call-with-values car cdr caar cadr cdar cddr
   caaar caadr cadar caddr cdaar cdadr cddar cdddr caaaar caaadr caadar caaddr cadaar
   cadadr caddar cadddr cdaaar cdaadr cdadar cdaddr cddaar cddadr cdddar cddddr
   ceiling char? char->integer char=? char<? char>? char<=? char>=?
   complex? cons cos 
   denominator dynamic-wind 
   eq? equal? eqv? even? exact? exp expt floor for-each
   gcd imag-part inexact? integer->char integer?
   lcm length list list->string
   list->vector list-ref list-tail list? log magnitude make-polar
   make-rectangular make-string make-vector map max min
   negative? not null? number->string number? numerator
   odd? pair? 
   positive? procedure? rational? rationalize
   real-part real? reverse round
   sin sqrt string string->list string->number string->symbol
   string-append 
   string-copy string-length string-ref string<=? string<?
   string=? string>=? string>? string? substring symbol->string symbol? tan
   truncate values vector vector->list
   vector-fill! vector-length vector-ref vector-set! vector? zero?
   
   ;; R6RS additional procedures:
   
   real-valued? rational-valued? integer-valued? exact inexact finite? infinite?
   nan? div mod div-and-mod div0 mod0 div0-and-mod0 exact-integer-sqrt boolean=?
   symbol=? string-for-each vector-map vector-for-each 
   (rename (r6rs#error error) (r6rs#assertion-violation assertion-violation))
   call/cc)
  
  (import (except (core primitives) _ ...)     
          (core let)                          
          (core derived)             
          (core quasiquote)        
          (core let-values)
          (for (core syntax-rules)      expand)   
          (for (core identifier-syntax) expand)
          (for (only (core primitives) _ ... set!) expand)
	  ;(only (rnrs exceptions) raise)
	  ;(only (rnrs conditions) condition make-error make-message-condition make-who-condition make-irritants-condition)
          (r5rs)
	  (gambit debug)
	  (primitives 
           
           ;; R6RS additional procedures:
           
           finite? infinite? nan? integer-sqrt r6rs#assertion-violation r6rs#error))
           
           ;; R5RS primitives:
           
           ;* + - / < <= = > >= abs acos append apply asin atan angle 
           ;boolean? call-with-current-continuation 
           ;call-with-values car cdr caar cadr cdar cddr
           ;caaar caadr cadar caddr cdaar cdadr cddar cdddr caaaar caaadr caadar caaddr cadaar
           ;cadadr caddar cadddr cdaaar cdaadr cdadar cdaddr cddaar cddadr cdddar cddddr
           ;ceiling char? char->integer char=? char<? char>? char<=? char>=?
           ;complex? cons cos 
           ;denominator dynamic-wind 
           ;eq? equal? eqv? even? exact? exp expt floor for-each
           ;gcd imag-part inexact? integer->char integer?
           ;lcm length list list->string
           ;list->vector list-ref list-tail list? log magnitude make-polar
           ;make-rectangular make-string make-vector map max min
           ;negative? not null? number->string number? numerator
           ;odd? pair? 
           ;positive? procedure? rational? rationalize
           ;real-part real? reverse round
           ;sin sqrt string string->list string->number string->symbol
           ;string-append 
           ;string-copy string-length string-ref string<=? string<?
           ;string=? string>=? string>? string? substring symbol->string symbol? tan
           ;truncate values vector vector->list
           ;vector-fill! vector-length vector-ref vector-set! vector? zero?
           

  (define (make-for-each name message length ref)
      (lambda (proc . seqs)
	(if (apply = (map length seqs))
	(let loop ((i 0))
	    (if (< i (length (car seqs)))
		(begin
			(apply proc (map (lambda (s) (ref s i)) seqs))
			(loop (+ i 1)))))
	(r6rs#assertion-violation name message seqs))))

  (define string-for-each (make-for-each 'string-for-each "Strings must have the same length!" string-length string-ref))
  (define vector-for-each (make-for-each 'vector-for-each "Vectors must have the same length!" vector-length vector-ref))
   
	(define (vector-map proc . vectors)
	 (if (apply = (map vector-length vectors))
	  (let loop ((i 0)
		     (result (vector (vector-length (car vectors)))))
	   (if (< i (vector-length result))
	    (begin
	     (vector-set! result i (apply proc (map (lambda (s) (vector-ref s i)))))
	     (loop (+ i 1)))
	    result))
	  (r6rs#assertion-violation 'vector-map "Vectors must have the same length!" vectors)))

  (define (symbol=? symbol1 symbol2 . symbols)
     (cond 
	((null? symbols) (eq? symbol1 symbol2))
	((null? (cdr symbols)) (and (eq? symbol1 symbol2) (eq? symbol1 (car symbols))))
	(else (and (eq? symbol1 symbol2) (apply symbol=? symbols)))))

  (define (boolean=? boolean1 boolean2 . booleans)
     (cond 
	((null? booleans) (eq? boolean1 boolean2))
	((null? (cdr booleans)) (and (eq? boolean1 boolean2) (eq? boolean1 (car booleans))))
	(else (and (eq? boolean1 boolean2) (apply boolean=? booleans)))))

  (define call/cc call-with-current-continuation)

  ; now defined as a native function
  #;(define (error who message . irritants)
    (raise
      (if who
	(condition (make-who-condition who)
		   (make-message-condition message)
		   (make-error)
		   (make-irritants-condition irritants))
	(condition
	  (make-message-condition message)
	  (make-error)
	  (make-irritants-condition irritants)))))

  
    (define-syntax assert
      (syntax-rules ()
        ((_ expression)
         (if (not expression)
             (r6rs#assertion-violation #f "assertion failed" 'expression)))))

;div and mod
      (define (div x y)
	(cond
	   ((zero? y) (r6rs#assertion-violation 'div "Second argument cannot be zero!" y))
	   ((and (negative? x) (negative? y)) (- (div x (- y))))
	   (else
	    (let ((n (* (numerator x) (denominator y)))
		  (d (* (denominator x) (numerator y))))
		(if (negative? n)
		    (- (quotient (- d n 1) d))
		    (quotient n d))))
	   ))
	
      (define (div0 x y)
	(cond
	   ((zero? y) (r6rs#assertion-violation 'div0 "Second argument cannot be zero!" y))
	   ((positive? x) (div x y))
	   (else (- (div (- x) y)))))

     (define (div-and-mod x y)
	(if (zero? y) (r6rs#assertion-violation 'div-and-mod "Second argument cannot be zero!" y)
	(let ((d (div x y))) (values d (- x (* d y)))))) 
		 
     (define (div0-and-mod0 x y)
	   (if (zero? y) (r6rs#assertion-violation 'div0-and-mod0 "Second argument cannot be zero!" y)
	     (let ((d (div0 x y))) (values d (- x (* d y)))))) 
	 
     (define (mod x y) 
	(if (zero? y) (r6rs#assertion-violation 'mod "Second argument cannot be zero!" y)
	   (call-with-values 
		(lambda () (div-and-mod x y))
		(lambda (d m) m))))

     (define (mod0 x y) 
	(if (zero? y) (r6rs#assertion-violation 'mod0 "Second argument cannot be zero!" y)
	   (call-with-values 
		(lambda () (div0-and-mod0 x y))
		(lambda (d m) m))))	

   (define (exact-integer-sqrt k)
	(let  ((s (integer-sqrt k)))
	    (values s (- k s))))

   (define (real-valued? k)
  	(if (complex? k) 
		(and (zero? (imag-part k)) (real? (real-part k) )) 
		(real? k)))
   
   (define (rational-valued? k)
	(if (complex? k)
		(and (zero? (imag-part k)) (rational? (real-part k)))
		(rational? k)))

   (define (integer-valued? k)
	(if (complex? k)
		(and (zero? (imag-part k)) (integer? (real-part k)))
		(integer? k)))
   (define inexact exact->inexact)
   (define exact inexact->exact)
  ) ;; rnrs base


