(library (rnrs conditions (6))
	 (export (rename (r6rs#&condition &condition) (r6rs#condition condition) (r6rs#simple-conditions simple-conditions)) 
		 condition? condition-predicate condition-accessor
		 define-condition-type
		 (rename (r6rs#&message &message) (r6rs#make-message-condition make-message-condition)) message-condition? condition-message
		 &warning make-warning warning?
		 (rename (r6rs#&serious &serious)) make-serious-condition serious-condition?
		 (rename (r6rs#&error &error) (r6rs#make-error make-error)) error?
		 (rename (r6rs#&violation &violation)) make-violation violation?
		 (rename (r6rs#&assertion &assertion) (r6rs#make-assertion-violation make-assertion-violation)) assertion-violation?
		 (rename (r6rs#&irritants &irritants) (r6rs#make-irritants-condition make-irritants-condition)) irritants-condition? condition-irritants
		 (rename (r6rs#&who &who) (r6rs#make-who-condition make-who-condition)) who-condition? condition-who
		 &non-continuable make-non-continuable-violation non-continuable-violation?
		 &implementation-restriction make-implementation-restriction-violation implementation-restriction-violation?
		 &lexical make-lexical-violation lexical-violation?
		 (rename (r6rs#&syntax &syntax) (r6rs#make-syntax-violation make-syntax-violation)) syntax-violation-form syntax-violation-subform
		 &undefined make-undefined-violation undefined-violation?)
	 (import
	   (for (core primitives) expand run)
	   (for (core syntax-rules) expand run)
	   (for (core derived) expand run)
	   (core let)
	   (for (only (core records procedural) 
		      make-rtd rtd-constructor rtd-predicate rtd-accessor) expand run)
	   (primitives pair? null? car cdr list
		       r6rs#&condition r6rs#&serious r6rs#&violation r6rs#&assertion r6rs#&who r6rs#&message r6rs#&irritants r6rs#&syntax r6rs#&error
		       r6rs#condition r6rs#simple-conditions r6rs#make-assertion-violation r6rs#make-syntax-violation r6rs#make-error
		       r6rs#make-who-condition r6rs#make-message-condition r6rs#make-irritants-condition))

	 (define-syntax define-condition-type
	   (syntax-rules ()
		((_ condition-type supertype constructor predicate (field accessor) ...)
		 (begin
		   (define condition-type (make-rtd 'condition-type '#((immutable field) ...) supertype))
		   (define constructor (rtd-constructor condition-type))
		   (define predicate (condition-predicate condition-type))
		   (define accessor (condition-accessor condition-type 
							(rtd-accessor condition-type 'field))) ...))))
	 
	 (define condition? (rtd-predicate r6rs#&condition))
	   
	 (define (condition-predicate rtd)
	   (define predicate (rtd-predicate rtd))
	   (lambda (x)
	     (let recur ((conditions (r6rs#simple-conditions x)))
	       (if (null? conditions) #f
		 (if (predicate (car conditions)) #t
		   (recur (cdr conditions)))))))

	 ;should the accessor throw an error if the condition is not the proper type?
	 (define (condition-accessor rtd proc)
	   (define predicate (rtd-predicate rtd))
	   (lambda (x)
	     (let recur ((conditions (r6rs#simple-conditions x)))
	       (if (pair? conditions)
		 (if (predicate (car conditions))
		   (proc (car conditions))
		   (recur (cdr conditions)))))))

	 ;standard conditions
	 (define-condition-type &warning r6rs#&condition make-warning warning?)
	 (define make-serious-condition (rtd-constructor r6rs#&serious))
	 (define serious-condition? (condition-predicate r6rs#&serious))
	 (define error? (condition-predicate r6rs#&error))
         ;(define-condition-type &error &serious make-error error?)
	 (define make-violation (rtd-constructor r6rs#&violation))
	 (define violation? (condition-predicate r6rs#&violation))
	 (define assertion-violation? (condition-predicate r6rs#&assertion))
	 (define-condition-type &lexical r6rs#&violation make-lexical-violation lexical-violation?)
	 (define-condition-type &undefined r6rs#&violation make-undefined-violation undefined-violation?)
	 (define who-condition? (condition-predicate r6rs#&who))
	 (define condition-who (condition-accessor r6rs#&who (rtd-accessor r6rs#&who 'who)))
	 (define message-condition? (condition-predicate r6rs#&message))
	 (define condition-message (condition-accessor r6rs#&message (rtd-accessor r6rs#&message 'message)))
	 (define irritants-condition? (condition-predicate r6rs#&irritants))
	 (define condition-irritants (condition-accessor r6rs#&irritants (rtd-accessor r6rs#&irritants 'irritants)))

	 (define-condition-type &non-continuable r6rs#&violation 
				make-non-continuable-violation non-continuable-violation?)
	 (define-condition-type &implementation-restriction r6rs#&violation 
				make-implementation-restriction-violation 
				implementation-restriction-violation?)
	 (define syntax-violation? (condition-predicate r6rs#&syntax))
	 (define syntax-violation-form (condition-accessor r6rs#&syntax (rtd-accessor r6rs#&syntax 'form)))
	 (define syntax-violation-subform (condition-accessor r6rs#&syntax (rtd-accessor r6rs#&syntax 'subform)))

)
