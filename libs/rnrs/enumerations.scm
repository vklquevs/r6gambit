(library (rnrs enums (6))
	(export make-enumeration enum-set-universe enum-set-indexer enum-set-constructor enum-set->list enum-set-member? enum-set-subset? enum-set=? enum-set-union enum-set-intersection enum-set-difference enum-set-complement enum-set-projection define-enumeration)
	(import (rnrs base) (rnrs lists) (rnrs control) (core records procedural))
;for now I'm creating the records with the procedural syntax since srfi 99 doesn't come into being until later in the process
  
  (define enumeration-type (make-rtd 'enumeration-type '#((immutable universe))))
  (define make-enumeration-type (rtd-constructor enumeration-type))
  (define enumeration-type-universe (rtd-accessor enumeration-type 'universe))

  (define enum-set (make-rtd 'enum-set '#((immutable type) (immutable members))))
  (define make-enum-set (rtd-constructor enum-set))
  (define enum-set? (rtd-predicate enum-set))
  (define enum-set-type (rtd-accessor enum-set 'type))
  (define enum-set-members (rtd-accessor enum-set 'members))
  
	
  (define (make-enumeration symbols)
     (make-enum-set (make-enumeration-type symbols) symbols))

 (define (enum-set-universe enum-set)
    (unless (enum-set? enum-set) (assertion-violation 'enum-set-universe "argument must be an enum-set" enum-set))
    (enumeration-type-universe (enum-set-type enum-set)))
  
(define (enum-set-indexer set)
  (let* ((symbols (enum-set->list
                    (enum-set-universe set)))
         (cardinality (length symbols)))
    (lambda (x)
      (cond
       ((memq x symbols)
        => (lambda (probe)
             (- cardinality (length probe))))
       (else #f)))))

(define (enum-set-constructor enum-set)
  (unless (enum-set? enum-set) (assertion-violation 'enum-set-constructor "argument must be an enum-set" enum-set))
  (let* ((type (enum-set-type enum-set))
        (universe (enum-set-universe enum-set))
        (universe? (lambda (sym) (memq sym universe))))
   (lambda (symbols)
      (make-enum-set type (filter universe? symbols)))))

(define (enum-set->list enum-set)
   (unless (enum-set? enum-set) (assertion-violation 'enum-set->list "argument must be an enum-set" enum-set))
   (enum-set-members enum-set))

(define (enum-set-member? sym enum-set)
   (unless (enum-set? enum-set) (assertion-violation 'enum-set-member? "argument must be an enum-set" enum-set))
   (and (memq sym (enum-set-members enum-set)) #t))

(define (enum-set-subset? enum-set1 enum-set2)
 (unless (enum-set? enum-set1) (assertion-violation 'enum-set-subset? "argument must be an enum-set" enum-set1))
 (unless (enum-set? enum-set2) (assertion-violation 'enum-set-subset? "argument must be an enum-set" enum-set2))
 (let ((enum-set1-universe (enum-set-universe enum-set1))
       (enum-set2-universe (enum-set-universe enum-set2)))
  (and
   (for-all (lambda (sym) (memq sym enum-set1-universe)) enum-set2-universe)
   (for-all (lambda (sym) (enum-set-member? sym enum-set2)) (enum-set-members enum-set1)))))

(define (enum-set=? enum-set1 enum-set2)
   (unless (enum-set? enum-set1) (assertion-violation 'enum-set=? "argument must be an enum-set" enum-set1))
   (unless (enum-set? enum-set2) (assertion-violation 'enum-set=? "argument must be an enum-set" enum-set2))
   (and (enum-set-subset? enum-set1 enum-set2) (enum-set-subset? enum-set2 enum-set1)))

(define (enum-set-union enum-set1 enum-set2)
   (unless (enum-set? enum-set1) (assertion-violation 'enum-set-union "argument must be an enum-set" enum-set1))
   (unless (enum-set? enum-set2) (assertion-violation 'enum-set-union "argument must be an enum-set" enum-set2))
   (unless (eq? (enum-set-type enum-set1) (enum-set-type enum-set2)) 
	(assertion-violation 'enum-set-union "Enum-set1 and enum-set2 must be enumeration sets that have the same enumeration type." enum-set1 enum-set2))
    (make-enum-set (enum-set-type enum-set1) 
	(filter (lambda (sym) 
			(or (enum-set-member? sym enum-set1) 
			(enum-set-member? sym enum-set2))) 
		(enum-set-universe enum-set1))))

 (define (enum-set-intersection enum-set1 enum-set2)
   (unless (enum-set? enum-set1) (assertion-violation 'enum-set-intersection "argument must be an enum-set" enum-set1))
   (unless (enum-set? enum-set2) (assertion-violation 'enum-set-intersection "argument must be an enum-set" enum-set2))
   (unless (eq? (enum-set-type enum-set1) (enum-set-type enum-set2)) 
	(assertion-violation 'enum-set-intersection "Enum-set1 and enum-set2 must be enumeration sets that have the same enumeration type." enum-set1 enum-set2))
    (make-enum-set (enum-set-type enum-set1) 
	(filter (lambda (sym) (enum-set-member? sym enum-set2))
                (enum-set-members enum-set1))))
		
(define (enum-set-difference enum-set1 enum-set2)
   (unless (enum-set? enum-set1) (assertion-violation 'enum-set-difference "argument must be an enum-set" enum-set1))
   (unless (enum-set? enum-set2) (assertion-violation 'enum-set-difference "argument must be an enum-set" enum-set2))
   (unless (eq? (enum-set-type enum-set1) (enum-set-type enum-set2)) 
	(assertion-violation 'enum-set-differnence "Enum-set1 and enum-set2 must be enumeration sets that have the same enumeration type." enum-set1 enum-set2))
    (make-enum-set (enum-set-type enum-set1) 
	(filter (lambda (sym) (not (enum-set-member? sym enum-set2)))
                (enum-set-members enum-set1))))


(define (enum-set-complement enum-set)
   (unless (enum-set? enum-set) (assertion-violation 'enum-set-difference "argument must be an enum-set" enum-set))
    (make-enum-set (enum-set-type enum-set) 
	(filter (lambda (sym) (not (enum-set-member? sym enum-set)))
                (enum-set-universe enum-set))))

(define (enum-set-projection enum-set1 enum-set2)
 (unless (enum-set? enum-set1) (assertion-violation 'enum-set-difference "argument must be an enum-set" enum-set1))
 (unless (enum-set? enum-set2) (assertion-violation 'enum-set-difference "argument must be an enum-set" enum-set2))
 (if (eq? (enum-set-type enum-set1) (enum-set-type enum-set2))
  enum-set1
  (let ((universe (enum-set-universe enum-set2)))
   (make-enum-set (enum-set-type enum-set2)
    (filter (lambda (sym) (memq sym universe))
     (enum-set-members enum-set1))))))


(define-syntax define-enumeration
  (syntax-rules ()
   ((_ type-name (symbol1 ...) set-constructor-syntax)
    (begin (define-syntax type-name
             (syntax-rules (symbol1 ...)
              ((_ symbol1) 'symbol1)
              ...))
           (define hidden-name (make-enumeration '(symbol1 ...)))
           (define-syntax set-constructor-syntax
             (syntax-rules ()
              ((_ sym1 (... ...))
               ((enum-set-constructor hidden-name)
                (list (type-name sym1) (... ...))))))))))

    
)

 
	   
