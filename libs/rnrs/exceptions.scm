(library (rnrs exceptions (6))
	 (export with-exception-handler guard (rename (abort raise) (raise raise-continuable)))
	 (import (r5rs) (primitives with-exception-handler abort raise))

(define-syntax guard
   (syntax-rules ()
	((guard (var cond-clauses ...) body ...)
		(with-exception-handler (lambda (var) (cond cond-clauses ...)) (lambda () body ...)))))

)
	
