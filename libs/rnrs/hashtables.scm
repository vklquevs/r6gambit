(library (rnrs hashtables (6))
  (export make-eq-hashtable make-eqv-hashtable make-hashtable hashtable-ref (rename (hash-table? hashtable?) (hash-table-size hashtable-size)  (hash-table-set! hashtable-set!) (hash-table-delete! hashtable-delete!) (hash-table-exists? hashtable-contains?) (hash-table-update! hashtable-update!)  (hash-table-clear! hashtable-clear!)) hashtable-copy hashtable-keys hashtable-entries 
	;inspection
	hashtable-hash-function
	(rename (hash-table-equivalence-function hashtable-equivalence-function)
	        (hash-table-mutable? hashtable-mutable?)) 
	;hashes
	(rename (hash equal-hash)) symbol-hash string-hash string-ci-hash )
  (import (core hashtables) (rnrs base) (rnrs control))

	(define make-eq-hashtable
		(case-lambda
		   (() (make-hash-table eq? hash-by-identity ))
		   ((size) (make-hash-table eq? hash-by-identity size: size))))

        (define make-eqv-hashtable
		(case-lambda 
			((size) (make-hash-table eqv? eqv?-hash size: size))
			(() (make-hash-table eqv? eqv?-hash))))

	(define make-hashtable 
		(case-lambda
			((hash-function equiv size) (make-hash-table equiv hash-function size: size))
			((hash-function equiv) (make-hash-table equiv hash-function))))

	
	


	;don't rename so that error messages make better sense.
        (define (hashtable-ref hashtable key default) (hash-table-ref/default hashtable key default))
		
	(define (hashtable-copy hashtable . mutable)
	   (let ((result (hash-table-copy hashtable)))
	     (hash-table-mutable?-set! result (or (null? mutable) (car mutable)))
	     result)) 
	

	(define (hashtable-keys hashtable)
	   (define result (make-vector (hash-table-size hashtable)))
	   (define index 0)
	   (hash-table-walk
		hashtable
		(lambda (key value) 
			(vector-set! result index key) 
			(set! index (+ index 1))))
	   result)

	(define (hashtable-entries hashtable)
	   (define keys (make-vector (hash-table-size hashtable)))
	   (define vals (make-vector (hash-table-size hashtable)))
	   (define index 0)
	   (hash-table-walk
		hashtable
		(lambda (key value)
			(vector-set! keys index key)
			(vector-set! vals index value)
			(set! index (+ index 1))))
	   (values keys vals))
	;inspection

	(define (hashtable-hash-function hashtable)
		(let ((result (hash-table-hash-function hashtable)))
			(and (not (eq? result hash-by-identity))
			     (not (eq? result eqv?-hash))
			     result)))

	

)
