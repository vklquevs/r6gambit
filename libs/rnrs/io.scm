;this is used by (rnrs io ports), (rnrs io simple), and (rnrs files). It is not part of the standard but I use the same rnrs namespace for convenience.
(library (rnrs io conditions)
	(export make-i/o-error i/o-error? make-i/o-read-error i/o-read-error? make-i/o-write-error i/o-write-error? make-i/o-invalid-position-error i/o-invalid-position-error? i/o-error-position
make-i/o-filename-error i/o-filename-error? i/o-error-filename make-i/o-file-protection-error i/o-file-protection-error? make-i/o-file-is-read-only-error i/o-file-is-read-only-error?
make-i/o-file-already-exists-error i/o-file-already-exists-error? make-i/o-file-does-not-exist-error i/o-file-does-not-exist-error? make-i/o-port-error i/o-port-error? i/o-error-port)
	(import (rnrs conditions (6)))
	
	(define-condition-type &i/o &error make-i/o-error i/o-error?)
	(define-condition-type &i/o-read &i/o make-i/o-read-error i/o-read-error?)
	(define-condition-type &i/o-write &i/o make-i/o-write-error i/o-write-error?)
	(define-condition-type &i/o-invalid-position &i/o make-i/o-invalid-position-error i/o-invalid-position-error?  (position i/o-error-position))
	(define-condition-type &i/o-filename &i/o make-i/o-filename-error i/o-filename-error?  (filename i/o-error-filename))
	(define-condition-type &i/o-file-protection &i/o-filename make-i/o-file-protection-error i/o-file-protection-error?)
	(define-condition-type &i/o-file-is-read-only &i/o-file-protection make-i/o-file-is-read-only-error i/o-file-is-read-only-error?)
	(define-condition-type &i/o-file-already-exists &i/o-filename make-i/o-file-already-exists-error i/o-file-already-exists-error?)
	(define-condition-type &i/o-file-does-not-exist &i/o-filename make-i/o-file-does-not-exist-error i/o-file-does-not-exist-error?)
	(define-condition-type &i/o-port &i/o make-i/o-port-error i/o-port-error?  (port i/o-error-port)))
	
(library (rnrs io ports (6))
  (export eof-object eof-object? port? input-port? output-port? close-port call-with-string-output-port open-string-output-port (rename (open-input-string open-string-input-port)) call-with-port
;conditions
   make-i/o-error i/o-error? make-i/o-read-error i/o-read-error? make-i/o-write-error i/o-write-error? make-i/o-invalid-position-error i/o-invalid-position-error? i/o-error-position
make-i/o-filename-error i/o-filename-error? i/o-error-filename make-i/o-file-protection-error i/o-file-protection-error? make-i/o-file-is-read-only-error i/o-file-is-read-only-error?
make-i/o-file-already-exists-error i/o-file-already-exists-error? make-i/o-file-does-not-exist-error i/o-file-does-not-exist-error? make-i/o-port-error i/o-port-error? i/o-error-port
;text port functions
(rename (read-char get-char) (peek-char lookahead-char)) get-string-n get-string-n! get-string-all get-line get-datum
;binary port functions
get-u8 put-u8 put-bytevector
)
  (import (gambit io)
	  (rnrs base)
	  (rnrs bytevectors)
	  (rnrs control)
	  (rnrs io conditions))
  
  (define (eof-object) #!eof)
  ;text port functions
  (define (get-string-n port count)
	(let* ((result (make-string count))
	       (count (get-string-n! port result 0 count)))
	   (substring result 0 count)))
  (define (get-string-n! port s start count)
	(read-substring s start count port))
  (define (get-string-all port) (read-line port #f))
  (define (get-line port) (read-line port #\linefeed))
  (define (get-datum port) (read port))
  (define (get-u8 port) (read-u8 port))
  (define (put-u8 port u8) (write-u8 u8 port))
  (define put-bytevector
	(case-lambda
		((port bytevector) (write-subu8vector bytevector 0 (bytevector-length bytevector) port))
		((port bytevector start) (write-subu8vector bytevector start (bytevector-length bytevector) port))
		((port bytevector start count) (write-subu8vector bytevector start (+ start count) port))))
  (define (open-string-output-port)
	(let ((port (open-output-string)))
	   (values port (lambda () (get-output-string port)))))

  (define (call-with-string-output-port proc) (call-with-output-string '() proc))
  
  (define (call-with-port port proc)
	(dynamic-wind
		(lambda () #f)
		(lambda () (proc port))
		(lambda () (close-port port))))
  


)

(library (rnrs io simple (6))
  (export
    eof-object eof-object? call-with-input-file call-with-output-file input-port? output-port?  current-input-port current-output-port current-error-port with-input-from-file with-output-to-file open-input-file open-output-file close-input-port close-output-port read-char peek-char read display  newline write write-char
;conditions
   make-i/o-error i/o-error? make-i/o-read-error i/o-read-error? make-i/o-write-error i/o-write-error? make-i/o-invalid-position-error i/o-invalid-position-error? i/o-error-position
make-i/o-filename-error i/o-filename-error? i/o-error-filename make-i/o-file-protection-error i/o-file-protection-error? make-i/o-file-is-read-only-error i/o-file-is-read-only-error?
make-i/o-file-already-exists-error i/o-file-already-exists-error? make-i/o-file-does-not-exist-error i/o-file-does-not-exist-error? make-i/o-port-error i/o-port-error? i/o-error-port

)
  (import (gambit io)
	  (rnrs base)
	  (rnrs io conditions))

  (define (eof-object) #!eof))

