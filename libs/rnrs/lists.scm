;derived from srfi-1
(library (rnrs lists (6))
  (export find for-all exists filter partition fold-left fold-right
          remp remove remq remv 
	  memp member memv memq
          assp assoc assv assq
	  cons*)
  (import (primitives member memv memq assoc assv assq)
	  (rnrs base))

  (define (filter pred lis)                       ; Sleazing with EQ? makes this
    ;(check-arg procedure? pred filter)            ; one faster.
    (let recur ((lis lis)) 
      (if (null? lis) lis             
	(let ((head (car lis))
	      (tail (cdr lis)))
	  (if (pred head)
	    (let ((new-tail (recur tail)))    ; Replicate the RECUR call so
	      (if (eq? tail new-tail) lis
		(cons head new-tail)))
	    (recur tail))))))
  
  (define (partition pred lis)
    ;(check-arg procedure? pred partition)
    (let recur ((lis lis))
      (if (null? lis) (values lis lis) 
	(let ((elt (car lis))
	      (tail (cdr lis)))
	  (let-values ([(in out) (recur tail)])
		   (if (pred elt)
		     (values (if (pair? out) (cons elt in) lis) out)
		     (values in (if (pair? in) (cons elt out) lis))))))))
  
  (define (remp pred l) (filter (lambda (x) (not (pred x))) l))
  (define (remove obj l) (filter (lambda (x) (not (equal? obj x))) l))
  (define (remv obj l) (filter (lambda (x) (not (eqv? obj x))) l))
  (define (remq obj l) (filter (lambda (x) (not (eq? obj x))) l))

  (define (cons* first . rest)
    (let recur ((x first) (rest rest))
      (if (pair? rest)
	(cons x (recur (car rest) (cdr rest)))
	x)))

  (define (find pred list)
    (cond ((memp pred list) => car)
	  (else #f)))
  
  (define (memp pred list)
    ;(check-arg procedure? pred find-tail)
    (let lp ((list list))
      (and (not (null? list))
	   (if (pred (car list)) list
	     (lp (cdr list))))))

(define (assp proc list) (find (lambda (entry) (proc (car entry))) list))

;;; LISTS is a (not very long) non-empty list of lists.
;;; Return two lists: the cars & the cdrs of the lists.
;;; However, if any of the lists is empty, just abort and return [() ()].
(define (car+cdr pair) (values (car pair) (cdr pair)))

(define (%cars+cdrs lists)
  (call-with-current-continuation
    (lambda (abort)
      (let recur ((lists lists))
        (if (pair? lists)
            (let-values ([(list other-lists) (car+cdr lists)])
              (if (null? list) (abort '() '()) ; LIST is empty -- bail out
		(let-values ([(a d) (car+cdr list)]
			     [(cars cdrs) (recur other-lists)])
                      (values (cons a cars) (cons d cdrs)))))
            (values '() '()))))))
          
(define (exists pred lis1 . lists)
  ;(check-arg procedure? pred any)
  (if (pair? lists)

    ;; N-ary case
    (let-values ([(heads tails) (%cars+cdrs (cons lis1 lists))])
	     (and (pair? heads)
		  (let lp ((heads heads) (tails tails))
		    (let-values ([(next-heads next-tails) (%cars+cdrs tails)])
			     (if (pair? next-heads)
			       (or (apply pred heads) (lp next-heads next-tails))
			       (apply pred heads)))))) ; Last PRED app is tail call.

    ;; Fast path
    (and (not (null? lis1))
	 (let lp ((head (car lis1)) (tail (cdr lis1)))
	   (cond 
		((null? tail) (pred head))            ; Last PRED app is tail call.
		((pair? tail) (or (pred head) (lp (car tail) (cdr tail))))
		(else (assertion-violation 'exists "list must proper" lis1)))))))

(define (for-all pred lis1 . lists)
  ;(check-arg procedure? pred every)
  (if (pair? lists)

    ;; N-ary case
    (let-values ([(heads tails) (%cars+cdrs (cons lis1 lists))])
	     (or (not (pair? heads))
		 (let lp ((heads heads) (tails tails))
		   (let-values ([(next-heads next-tails) (%cars+cdrs tails)])
			    (if (pair? next-heads)
			      (and (apply pred heads) (lp next-heads next-tails))
			      (apply pred heads)))))) ; Last PRED app is tail call.

    ;; Fast path
    (or (null? lis1)
	(let lp ((head (car lis1))  (tail (cdr lis1)))
	  (cond 
		((null? tail) (pred head))     ; Last PRED app is tail call.
	    	((pair? tail) (and (pred head) (lp (car tail) (cdr tail))))
		(else (assertion-violation 'exists "list must proper" lis1))))))) 

(define (fold-left kons knil lis1 . lists)
;  (define (%cars+cdrs+ lists cars-final)
;    (call-with-current-continuation
;      (lambda (abort)
;	(let recur ((lists lists))
;	  (if (pair? lists)
;	    (let-values ([(list other-lists) (car+cdr lists)])
;		     (if (null? list) (abort '() '()) ; LIST is empty -- bail out
;		       (let-values ([(a d) (car+cdr list)]
;				    [(cars cdrs) (recur other-lists)])
;					 (values (cons a cars) (cons d cdrs)))))
;	    (values (list cars-final) '()))))))

  ;(check-arg procedure? kons fold)
  (if (pair? lists)
    (let lp ((lists (cons lis1 lists)) (ans knil))    ; N-ary case
      (let-values ([(cars cdrs) (%cars+cdrs lists)])
		  (if (null? cars) ans ; Done.
		    (lp cdrs (apply kons ans cars)))))

    (let lp ((lis lis1) (ans knil))                   ; Fast path
      (if (null? lis) ans
	(lp (cdr lis) (kons ans (car lis)))))))


(define (fold-right kons knil lis1 . lists)
  (define (%cdrs lists)
    (call-with-current-continuation
      (lambda (abort)
	(let recur ((lists lists))
	  (if (pair? lists)
	    (let ((lis (car lists)))
	      (if (null? lis) (abort '())
		(cons (cdr lis) (recur (cdr lists)))))
	    '())))))
  (define (%cars+ lists last-elt) ; (append! (map car lists) (list last-elt))
    (let recur ((lists lists))
      (if (pair? lists) (cons (caar lists) (recur (cdr lists))) (list last-elt))))

  ;(check-arg procedure? kons fold-right)
  (if (pair? lists)
    (let recur ((lists (cons lis1 lists)))            ; N-ary case
      (let ((cdrs (%cdrs lists)))
	(if (null? cdrs) knil
	  (apply kons (%cars+ lists (recur cdrs))))))

    (let recur ((lis lis1))                           ; Fast path
      (if (null? lis) knil
	(let ((head (car lis)))
	  (kons head (recur (cdr lis))))))))
)
