;; Nonstandard library for loading files into a
;; top-level interactive REPL environment.
;; The files may contain libraries in source form,
;; which are them dynamically loaded.  

(library (rnrs load)
  (export load)
  (import (rnrs)
          (primitives r6rs#repl))
  
  (define (load filename)
    (define (read-file fn)
      (let ((p (open-input-file fn)))
        (let f ((x (read p)))
          (if (eof-object? x)
              (begin (close-input-port p) '())
              (cons x
                    (f (read p)))))))
    (r6rs#repl (read-file filename)))
  )
  
