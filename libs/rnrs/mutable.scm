(library (rnrs mutable-pairs (6))
  (export set-car! set-cdr!)
  (import (primitives set-car! set-cdr!)))

(library (rnrs mutable-strings (6))
  (export string-set! string-fill!)
  (import (primitives string-set! string-fill!)))

