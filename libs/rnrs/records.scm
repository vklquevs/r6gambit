;this is a utility library that implements record-constructor-descriptors.
(library (rnrs records rcd)
   (export record-constructor-descriptor record-constructor-rtd record-constructor-pcd record-constructor-protocol record-constructor-maker record-constructor-descriptor?)
   (import (rnrs base) (core records procedural))

  (define record-constructor-descriptor 
	(make-rtd 'record-constructor-descriptor '#((immutable rtd) (immutable pcd) (immutable protocol) (immutable maker))))
  (define record-constructor-rtd (rtd-accessor record-constructor-descriptor 'rtd))
  (define record-constructor-pcd (rtd-accessor record-constructor-descriptor 'pcd))
  (define record-constructor-protocol (rtd-accessor record-constructor-descriptor 'protocol))
  (define record-constructor-maker (rtd-accessor record-constructor-descriptor 'maker))
  (define record-constructor-descriptor? (rtd-predicate record-constructor-descriptor))
)

(library (rnrs records procedural (6))
  (export
   make-record-type-descriptor (rename (rtd? record-type-descriptor?) (rtd-predicate record-predicate))
   make-record-constructor-descriptor record-constructor
   record-accessor record-mutator)
  (import 
    (rnrs base)
    (rnrs lists)
    (rnrs control)
    (rnrs eval)
    (core records procedural)
    (core records inspection)
    (rnrs records rcd) 
    (only (gambit extensions) gensym)
    (primitives record-accessor record-mutator))


   (define (make-record-type-descriptor name parent uid sealed? opaque? fields)
     (if uid
       (make-rtd name fields parent (and sealed? 'sealed) (and opaque? 'opaque) 'uid uid)
       (make-rtd name fields parent (and sealed? 'sealed) (and opaque? 'opaque) 'uid uid)))
 

  (define make-record-constructor-descriptor
     (let ((constructor (rtd-constructor record-constructor-descriptor))
           (compose (lambda (f g) (lambda (x) (f (g x)))))
	   (identity (lambda (f) f)))
           
	(lambda (rtd pcd protocol)
           (define parent (rtd-parent rtd))
	   (unless (rtd? rtd) (error 'make-record-constructor-descriptor "first argument must be a record-type-descriptor" rtd))
	   (when (and (not parent) pcd) (error 'make-record-constructor-descriptor "Cannot set a parent-constructor-descriptor for a base type"))
	   (unless (or (not pcd) (record-constructor-descriptor? pcd) (equal? parent (record-constructor-rtd pcd))) 
		(error 'make-record-constructor-descriptor "parent constructor descriptor must be a record-constructor-descriptor of the parent rtd or #f" pcd))
	   (unless (or (not protocol) (procedure? protocol)) (error 'make-record-constructor-descriptor "protocol must be a procedure or #f" protocol))
			

	  (let* ((pcd (if parent (or pcd (make-record-constructor-descriptor parent #f #f)) #f)) ;if there is a parent and a pcd, use that otherwise make a default one
	         (protocol (compose (or protocol (default-constructor-protocol rtd)) (or (and pcd (record-constructor-protocol pcd)) identity)))
		 (maker (constructor-maker rtd)))  	   
	   (constructor rtd pcd protocol maker)))))

  (define (gensyms l) (map (lambda (x) (gensym)) l))

  (define (default-constructor-protocol rtd)
	(let ((parent (rtd-parent rtd))
	      (fields (gensyms (vector->list (rtd-field-names rtd)))))
	   (if parent
		(let ((parent-fields (gensyms (vector->list (rtd-field-names parent)))))
		   (eval `(lambda (n) 
				(lambda ,(append parent-fields fields) 
					(let ((p (n ,@parent-fields)))
					    (p ,@fields)))) (environment '(rnrs base))))
		(eval `(lambda (p) (lambda ,fields (p ,@fields))) (environment '(rnrs base))))))

  (define (constructor-maker rtd)
     (let      ((constructor (gensym))
		(fields (let loop ((rtd rtd)) (cons (vector->list (rtd-field-names rtd)) (if (rtd-parent rtd) (loop (rtd-parent rtd)) '())))))
	(eval `(lambda (,constructor) ,(let loop ((fields fields))
				    `(lambda ,(car fields) ,(if (pair? (cdr fields)) (loop (cdr fields)) (cons constructor (apply append fields))))))
		(environment '(rnrs base)))))
		
  (define (record-constructor rtc)
      ((record-constructor-protocol rtc) ((record-constructor-maker rtc) (rtd-constructor (record-constructor-rtd rtc)))))
				   
)
(library (rnrs records inspection (6))
  (export
   record? record-rtd record-type-name record-type-parent record-type-uid
   record-type-generative? record-type-sealed? record-type-opaque?
   record-type-field-names record-field-mutable?)
  (import
    (rnrs base)
    (only (gambit extensions) uninterned-symbol?)
    (core records inspection)
    (primitives record-field-mutable?))
    
    (define record-type-name rtd-name)
    (define record-type-parent rtd-parent)
    (define record-type-uid rtd-uid)
    (define record-type-sealed? rtd-sealed?)
    (define record-type-opaque? rtd-opaque?)
    (define record-type-field-names rtd-field-names)
    (define record-type-generative? rtd-generative?) 
)

(library (rnrs records syntactic (6))
  (export define-record-type record-type-descriptor record-constructor-descriptor) ;fields mutable immutable parent protocol sealed opaque nongenerative parent-rtd)   
  (import (rnrs records procedural (6)) (only (rnrs records rcd) record-constructor-descriptor?) (for (rnrs base) run expand) (for (rnrs syntax-case) expand) )

  (define-syntax define-record-type
     (lambda (x)
       (let ((record-name #f)
	     (constructor-name #f)
             (predicate-name #f)
	     (sealed? #f)
             (opaque? #f)
             (parent-rtd #f)
             (parent-rcd #f)
	     (nongenerative #f)
             (protocol #f)
             (field-spec '())
	     (field-methods #f))           
             (define (process-record-clauses x record-name)
		(syntax-case x (parent protocol sealed opaque nongenerative parent-rtd)
		   ((_ (parent x))
		    (begin 
		    	(set! parent-rtd #'(record-type-descriptor x))
		    	(set! parent-rcd #'(record-constructor-descriptor x))
		    ))
		   ((_ (parent-rtd rtd rcd)) 
		    (begin
		    	(set! parent-rtd #'rtd)
		    	(set! parent-rcd #'rcd)
		    ))
	           ((_ (sealed b)) (set! sealed? (syntax->datum #'b)))
		   ((_ (opaque b))  (set! opaque? (syntax->datum #'b)))
                   ((_ (nongenerative uid)) (set! nongenerative (syntax->datum #'uid)))
		   ((_ (nongenerative)) (set! nongenerative #f))
		   ((_ (protocol p)) (set! protocol #'p))
		   ((_ (fields specs ...)) 
			(begin
			   (set! field-methods (process-fields #'(fields specs ...) record-name 0 '()))))
		   ((i clause clauses ...)
		      (begin
		          (process-record-clauses #'(i clause) record-name)
			  (process-record-clauses #'(i clauses ...) record-name)))))
              (define (process-fields x record-name index methods)
		  (define (default-accessor record-name field)
			(datum->syntax record-name (string->symbol (string-append (symbol->string (syntax->datum record-name)) "-" (symbol->string (syntax->datum field))))))
		  (define (default-mutator record-name field)
		   (datum->syntax record-name (string->symbol (string-append (symbol->string (syntax->datum record-name)) "-" (symbol->string (syntax->datum field))) "-set!")))
		  (syntax-case x (fields mutable immutable)
		     ((fields (immutable name accessor) specs ...)
		      (begin
		      	(set! field-spec (cons #''(immutable name) field-spec))
		      	(process-fields #'(fields specs ...) record-name (+ index 1)
				(cons #`(define accessor (record-accessor (record-type-descriptor #,record-name) #,(datum->syntax record-name index))) methods) )))
		     ((fields (mutable name accessor mutator) specs ...)
		      (begin
		      	(set! field-spec (cons #''(mutable name) field-spec))
		      	(process-fields #'(fields specs ...) record-name (+ index 1)
				(cons #`(begin
					   (define accessor (record-accessor (record-type-descriptor #,record-name) #,(datum->syntax record-name index)))
					   (define mutator (record-mutator (record-type-descriptor #,record-name)) #,(datum->syntax record-name index)))
				methods))))
		     ((fields (immutable name) specs ...)
		      (process-fields #`(fields (immutable name #,(default-accessor record-name #'name)) specs ...) record-name index methods))
		     ((fields (mutable name) specs ...)
		      (process-fields #`(fields (mutable name #,(default-accessor record-name #'name) #,(default-mutator record-name #'name)) specs ...) record-name index methods))
		     ((fields field specs ...)
		      (process-fields #'(fields (immutable field) specs ...) record-name index methods))
		     ((fields) #`(begin #,@methods))))
             (define (default-constructor record-name)
	 	(datum->syntax record-name (string->symbol (string-append "make-" (symbol->string (syntax->datum record-name))))))
	     (define (default-predicate record-name)
		(datum->syntax record-name (string->symbol (string-append (symbol->string (syntax->datum record-name)) "?"))))
		
	     (syntax-case x () 
                ((define-record-type (record-name constructor-name predicate-name) record-clauses ...)
		 (and (identifier? #'record-name) (identifier? #'constructor-name) (identifier? #'predicate-name))
		 (begin
		 	(process-record-clauses #'(process-record-clauses record-clauses ...) #'record-name)
		 #`(begin
		 	(define record-name 
			   (let ((rtd (make-record-type-descriptor 'record-name #,parent-rtd #,nongenerative #,sealed? #,opaque? (vector #,@field-spec))))
				(cons rtd (make-record-constructor-descriptor rtd #,parent-rcd #,protocol))))
			(define constructor-name (record-constructor (record-constructor-descriptor record-name)))
		        (define predicate-name (record-predicate (record-type-descriptor record-name)))
			#,field-methods)))
		((define-record-type record-name record-clauses ...)
		 #`(define-record-type (record-name #,(default-constructor #'record-name) #,(default-predicate #'record-name)) record-clauses ...))))))
		     

  (define-syntax record-type-descriptor
    (lambda (x)
      (syntax-case x ()
        ((_ name) (identifier? #'name) 
	#'(cond 
		((pair? name) (car name))
		((record-type-descriptor? name) name)
		(else (assertion-violation 'record-type-descriptor "unable to find record-type-descriptor!" 'name)))))))

  (define-syntax record-constructor-descriptor
    (lambda (x)
      (syntax-case x ()
        ((_ name) (identifier? #'name) 
	#'(cond
		((pair? name) (cdr name))
		((record-constructor-descriptor? name) name)
		(else (assertion-violation 'record-constructor-descriptor "unable to find record-type-constructor!" 'name)))))))
)
