(library (rnrs sorting (6))
  (export 
    (rename (list-merge-sort list-sort) (vector-merge-sort vector-sort) (vector-merge-sort! vector-sort!)))
  (import
    (rnrs base)
    (rnrs control)
    (only (gambit vectors) vector-copy))


;;; list merge & list merge-sort	-*- Scheme -*-
;;; Copyright (c) 1998 by Olin Shivers.
;;; This code is open-source; see the end of the file for porting and
;;; more copyright information.
;;; Olin Shivers

;;; Exports:
;;; (list-merge  < lis lis) -> list
;;; (list-merge! < lis lis) -> list
;;; (list-merge-sort  < lis) -> list
;;; (list-merge-sort! < lis) -> list

;;; A stable list merge sort of my own device
;;; Two variants: pure & destructive
;;;
;;; This list merge sort is opportunistic (a "natural" sort) -- it exploits
;;; existing order in the input set. Instead of recursing all the way down to
;;; individual elements, the leaves of the merge tree are maximal contiguous
;;; runs of elements from the input list. So the algorithm does very well on
;;; data that is mostly ordered, with a best-case time of O(n) when the input
;;; list is already completely sorted. In any event, worst-case time is
;;; O(n lg n).
;;;
;;; The destructive variant is "in place," meaning that it allocates no new
;;; cons cells at all; it just rearranges the pairs of the input list with
;;; SET-CDR! to order it.
;;;
;;; The interesting control structure is the combination recursion/iteration
;;; of the core GROW function that does an "opportunistic" DFS walk of the
;;; merge tree, adaptively subdividing in response to the length of the
;;; merges, without requiring any auxiliary data structures beyond the
;;; recursion stack. It's actually quite simple -- ten lines of code.
;;;	-Olin Shivers 10/20/98

;;; utilties
(define (has-element list index)
  (cond
    ((zero? index)
     (if (pair? list)
       (values #t (car list))
       (values #f #f)))
    ((null? list)
     (values #f #f))
    (else
      (has-element (cdr list) (- index 1)))))

(define (list-ref-or-default list index default)
  (call-with-values
    (lambda () (has-element list index))
    (lambda (has? maybe)
      (if has?
	maybe
	default))))

(define (vector-start+end vector maybe-start+end)
  (let ((start (list-ref-or-default maybe-start+end
				    0 0))
	(end (list-ref-or-default maybe-start+end
				  1 (vector-length vector))))
    (values start end)))

(define (vector-portion-copy! target src start end)
  (let ((len (- end start)))
    (do ((i (- len 1) (- i 1))
	 (j (- end 1) (- j 1)))
      ((< i 0))
      (vector-set! target i (vector-ref src j)))))


;;; (list-merge-sort < lis)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; A natural, stable list merge sort. 
;;; - natural: picks off maximal contiguous runs of pre-ordered data.
;;; - stable: won't invert the order of equal elements in the input list.

(define (list-merge-sort elt< lis)

  ;; (getrun lis) -> run runlen rest
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; Pick a run of non-decreasing data off of non-empty list LIS. 
  ;; Return the length of this run, and the following list.
  (define (getrun lis)
    (let lp ((ans '())  (i 1)  (prev (car lis))  (xs (cdr lis)))
      (if (pair? xs)
	  (let ((x (car xs)))
	    (if (elt< x prev) 
		(values (append-reverse ans (cons prev '())) i xs)
		(lp (cons prev ans) (+ i 1) x (cdr xs))))
	  (values (append-reverse ans (cons prev '())) i xs))))

  (define (append-reverse rev-head tail)
    (let lp ((rev-head rev-head) (tail tail))
      (if (null-list? rev-head) tail
	  (lp (cdr rev-head) (cons (car rev-head) tail)))))

  (define (null-list? l)
    (cond ((pair? l) #f)
	  ((null? l) #t)
	  (else (error "null-list?: argument out of domain" l))))

  ;; (merge a b) -> list
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; List merge -- stably merge lists A (length > 0) & B (length > 0).
  ;; This version requires up to |a|+|b| stack frames.
  (define (merge a b)
    (let recur ((x (car a)) (a a)
		(y (car b)) (b b))
      (if (elt< y x)
	  (cons y (let ((b (cdr b)))
		    (if (pair? b)
			(recur x a (car b) b)
			a)))
	  (cons x (let ((a (cdr a)))
		    (if (pair? a)
			(recur (car a) a y b)
			b))))))

  ;; (grow s ls ls2 u lw) -> [a la unused]
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; The core routine. Read the next 20 lines of comments & all is obvious.
  ;; - S is a sorted list of length LS > 1.
  ;; - LS2 is some power of two <= LS.
  ;; - U is an unsorted list.
  ;; - LW is a positive integer.
  ;; Starting with S, and taking data from U as needed, produce
  ;; a sorted list of *at least* length LW, if there's enough data
  ;; (LW <= LS + length(U)), or use all of U if not.
  ;;
  ;; GROW takes maximal contiguous runs of data from U at a time;
  ;; it is allowed to return a list *longer* than LW if it gets lucky
  ;; with a long run.
  ;;
  ;; The key idea: If you want a merge operation to "pay for itself," the two
  ;; lists being merged should be about the same length. Remember that.
  ;;
  ;; Returns:
  ;;   - A:      The result list
  ;;   - LA:     The length of the result list
  ;;   - UNUSED: The unused tail of U.

  (define (grow s ls ls2 u lw)	; The core of the sort algorithm.
    (if (or (<= lw ls) (not (pair? u)))	; Met quota or out of data?
	(values s ls u)			; If so, we're done.
	(let*-values (((ls2) (let lp ((ls2 ls2))
			(let ((ls2*2 (+ ls2 ls2)))
			  (if (<= ls2*2 ls) (lp ls2*2) ls2))))
	       ;; LS2 is now the largest power of two <= LS.
	       ;; (Just think of it as being roughly LS.)
	       ((r lr u2)  (getrun u))			; Get a run, then
	       ((t lt u3)  (grow r lr 1 u2 ls2))) 	; grow it up to be T.
	  (grow (merge s t) (+ ls lt)	 		; Merge S & T, 
		(+ ls2 ls2) u3 lw))))	     		;   and loop.

  ;; Note: (LENGTH LIS) or any constant guaranteed 
  ;; to be greater can be used in place of INFINITY.
  (if (pair? lis)				; Don't sort an empty list.
      (let*-values (((r lr tail)  (getrun lis))	; Pick off an initial run,
	     ((infinity)   #o100000000)		; then grow it up maximally.
	     ((a la v)     (grow r lr 1 tail infinity)))
	a)
      '()))



;;; (vector-merge-sort  < v [start end temp]) -> vector
;;; (vector-merge-sort! < v [start end temp]) -> unspecific
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Stable natural vector merge sort

(define (vector-merge-sort! < v . maybe-args)
  (call-with-values
   (lambda () (vector-start+end v maybe-args))
   (lambda (start end)
     (let ((temp (if (and (pair? maybe-args) ; kludge
			  (pair? (cdr maybe-args))
			  (pair? (cddr maybe-args)))
		     (caddr maybe-args)
		     (vector-copy v))))
       (%vector-merge-sort! < v start end temp)))))

(define (vector-merge-sort < v . maybe-args)
  (let ((ans (vector-copy v)))
    (apply vector-merge-sort! < ans maybe-args)
    ans))


;;; %VECTOR-MERGE-SORT! is not exported.
;;; Preconditions:
;;;   V TEMP vectors
;;;   START END fixnums
;;;   START END legal indices for V and TEMP
;;; If these preconditions are ensured by the cover functions, you
;;; can safely change this code to use unsafe fixnum arithmetic and vector
;;; indexing ops, for *huge* speedup.

;;; This merge sort is "opportunistic" -- the leaves of the merge tree are
;;; contiguous runs of already sorted elements in the vector. In the best
;;; case -- an already sorted vector -- it runs in linear time. Worst case
;;; is still O(n lg n) time.

(define (%vector-merge-sort! elt< v0 l r temp0)
  (define (xor a b) (not (eq? a b)))

  ;; Merge v1[l,l+len1) and v2[l+len1,l+len1+len2) into target[l,l+len1+len2)
  ;; Merge left-to-right, so that TEMP may be either V1 or V2
  ;; (that this is OK takes a little bit of thought).
  ;; V2=TARGET? is true if V2 and TARGET are the same, which allows
  ;; merge to punt the final blit half of the time.
  
  (define (merge target v1 v2 l len1 len2 v2=target?)
    (letrec ((vblit (lambda (fromv j i end)    ; Blit FROMV[J,END) to TARGET[I,?]
		      (let lp ((j j) (i i))    ; J < END. The final copy.
			(vector-set! target i (vector-ref fromv j))
			(let ((j (+ j 1)))
			  (if (< j end) (lp j (+ i 1))))))))
  
      (let* ((r1 (+ l  len1))
	     (r2 (+ r1 len2)))
							; Invariants:
	(let lp ((n l)					; N is next index of 
		 (j l)   (x (vector-ref v1 l))		;   TARGET to write.   
		 (k r1)  (y (vector-ref v2 r1)))	; X = V1[J]          
	  (let ((n+1 (+ n 1)))				; Y = V2[K]          
	    (if (elt< y x)
		(let ((k (+ k 1)))
		  (vector-set! target n y)
		  (if (< k r2)
		      (lp n+1 j x k (vector-ref v2 k))
		      (vblit v1 j n+1 r1)))
		(let ((j (+ j 1)))
		  (vector-set! target n x)
		  (if (< j r1)
		      (lp n+1 j (vector-ref v1 j) k y)
		      (if (not v2=target?) (vblit v2 k n+1 r2))))))))))
  

  ;; Might hack GETRUN so that if the run is short it pads it out to length
  ;; 10 with insert sort...
  
  ;; Precondition: l < r.
  (define (getrun v l r)
    (let lp ((i (+ l 1))  (x (vector-ref v l)))
      (if (>= i r)
	  (- i l)
	  (let ((y (vector-ref v i)))
	    (if (elt< y x)
		(- i l)
		(lp (+ i 1) y))))))
  
  ;; RECUR: Sort V0[L,L+LEN) for some LEN where 0 < WANT <= LEN <= (R-L).
  ;;   That is, sort *at least* WANT elements in V0 starting at index L.
  ;;   May put the result into either V0[L,L+LEN) or TEMP0[L,L+LEN).
  ;;   Must not alter either vector outside this range.
  ;;   Return:
  ;;     - LEN -- the number of values we sorted
  ;;     - ANSVEC -- the vector holding the value
  ;;     - ANS=V0? -- tells if ANSVEC is V0 or TEMP
  ;;
  ;; LP: V[L,L+PFXLEN) holds a sorted prefix of V0.
  ;;     TEMP = if V = V0 then TEMP0 else V0. (I.e., TEMP is the other vec.)
  ;;     PFXLEN2 is a power of 2 <= PFXLEN.
  ;;     Solve RECUR's problem.
  (if (< l r) ; Don't try to sort an empty range.
      (call-with-values
       (lambda ()
	 (let recur ((l l) (want (- r l)))
	   (let ((len (- r l)))
	     (let lp ((pfxlen (getrun v0 l r)) (pfxlen2 1)
		      (v v0) (temp temp0)
		      (v=v0? #t))
	       (if (or (>= pfxlen want) (= pfxlen len))
		   (values pfxlen v v=v0?)
		   (let ((pfxlen2 (let lp ((j pfxlen2))
				    (let ((j*2 (+ j j)))
				      (if (<= j pfxlen) (lp j*2) j))))
			 (tail-len (- len pfxlen)))
		     ;; PFXLEN2 is now the largest power of 2 <= PFXLEN.
		     ;; (Just think of it as being roughly PFXLEN.)
		     (call-with-values
		      (lambda ()
			(recur (+ pfxlen l) pfxlen2))
		      (lambda (nr-len nr-vec nrvec=v0?)
			(merge temp v nr-vec l pfxlen nr-len
			       (xor nrvec=v0? v=v0?))
			(lp (+ pfxlen nr-len) (+ pfxlen2 pfxlen2)
			    temp v (not v=v0?))))))))))
       (lambda (ignored-len ignored-ansvec ansvec=v0?)
	 (if (not ansvec=v0?) (vector-portion-copy! v0 temp0 l r))))))


;;; Copyright
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; This code is
;;;     Copyright (c) 1998 by Olin Shivers.
;;; The terms are: You may do as you please with this code, as long as
;;; you do not delete this notice or hold me responsible for any outcome
;;; related to its use.
;;;
;;; Blah blah blah. Don't you think source files should contain more lines
;;; of code than copyright notice?


;;; Code tuning & porting
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; This code is *tightly* bummed as far as I can go in portable Scheme.
;;;
;;; The two internal primitives that do the real work can be converted to
;;; use unsafe vector-indexing and fixnum-specific arithmetic ops *if* you
;;; alter the four small cover functions to enforce the invariants. This should
;;; provide *big* speedups. In fact, all the code bumming I've done pretty
;;; much disappears in the noise unless you have a good compiler and also
;;; can dump the vector-index checks and generic arithmetic -- so I've really
;;; just set things up for you to exploit.
;;;
;;; The optional-arg parsing, defaulting, and error checking is done with a
;;; portable R4RS macro. But if your Scheme has a faster mechanism (e.g., 
;;; Chez), you should definitely port over to it. Note that argument defaulting
;;; and error-checking are interleaved -- you don't have to error-check 
;;; defaulted START/END args to see if they are fixnums that are legal vector
;;; indices for the corresponding vector, etc.
)
