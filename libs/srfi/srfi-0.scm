;copied cond-expand from gambit's syntax-case system
;TODO use the one built into gambit or better yet create a new one
(library (srfi :0) 
	 (export cond-expand)
	 (import (rnrs base))

(define-syntax cond-expand
     (syntax-rules (and or not else srfi-0 srfi-30 gambit)
		   ((cond-expand) (syntax-error "Unfulfilled cond-expand"))
		   ((cond-expand (else body ...))
		    (begin body ...))
		   ((cond-expand ((and) body ...) more-clauses ...)
		    (begin body ...))
		   ((cond-expand ((and req1 req2 ...) body ...) more-clauses ...)
		    (cond-expand
		      (req1
			(cond-expand
			  ((and req2 ...) body ...)
			  more-clauses ...))
		      more-clauses ...))
		   ((cond-expand ((or) body ...) more-clauses ...)
		    (cond-expand more-clauses ...))
		   ((cond-expand ((or req1 req2 ...) body ...) more-clauses ...)
		    (cond-expand
		      (req1
			(begin body ...))
		      (else
			(cond-expand
			  ((or req2 ...) body ...)
			  more-clauses ...))))
		   ((cond-expand ((not req) body ...) more-clauses ...)
		    (cond-expand
		      (req
			(cond-expand more-clauses ...))
		      (else body ...)))
		   ((cond-expand (srfi-0 body ...) more-clauses ...)
		    (begin body ...))
		   ((cond-expand (gambit body ...) more-clauses ...)
		    (begin body ...))
		   ((cond-expand (feature-id body ...) more-clauses ...)
		    (cond-expand more-clauses ...))))
)
