;srfi-18 is a subset of srfi-21
(library (srfi :18 multithreading) 
	 (export current-thread make-thread thread?  thread-name thread-specific thread-specific-set!  thread-start!  thread-sleep!  thread-yield!  thread-terminate!  thread-join!  make-mutex mutex?  mutex-name mutex-specific mutex-specific-set!  mutex-state mutex-lock!  mutex-unlock!  condition-variable?  make-condition-variable condition-variable-name condition-variable-specific condition-variable-specific-set!  condition-variable-signal!  condition-variable-broadcast!  current-time time? time->seconds seconds->time current-exception-handler with-exception-handler raise join-timeout-exception? abandoned-mutex-exception? terminated-thread-exception? uncaught-exception? uncaught-exception-reason)
	 (import (srfi :21 real-time-multithreading)))

(library (srfi :18) 
	 (export current-thread make-thread thread?  thread-name thread-specific thread-specific-set!  thread-start!  thread-sleep!  thread-yield!  thread-terminate!  thread-join!  make-mutex mutex?  mutex-name mutex-specific mutex-specific-set!  mutex-state mutex-lock!  mutex-unlock!  condition-variable?  make-condition-variable condition-variable-name condition-variable-specific condition-variable-specific-set!  condition-variable-signal!  condition-variable-broadcast!  current-time time? time->seconds seconds->time current-exception-handler with-exception-handler raise join-timeout-exception? abandoned-mutex-exception? terminated-thread-exception? uncaught-exception? uncaught-exception-reason)
	 (import (srfi :18 multithreading)))
