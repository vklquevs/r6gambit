;;;AND-LET srfi-2
;;;This is copied from ssax library

;      AND-LET* -- an AND with local bindings, a guarded LET* special form
;
; AND-LET* (formerly know as LAND*) is a generalized AND: it evaluates
; a sequence of forms one after another till the first one that yields
; #f; the non-#f result of a form can be bound to a fresh variable and
; used in the subsequent forms.
; It is defined in SRFI-2 <http://srfi.schemers.org/srfi-2/>
; This macro re-writes the and-let* form into a combination of
; 'and' and 'let'.
; See vland.scm for the denotational semantics and
; extensive validation tests.
(library (srfi :2 and-let*)
	 (export and-let*)
	 (import (rnrs base))

(define-syntax and-let*
  (syntax-rules ()
    ((_ ()) #t)
    ((_ claws)    ; no body
       ; re-write (and-let* ((claw ... last-claw)) ) into
       ; (and-let* ((claw ...)) body) with 'body' derived from the last-claw
     (and-let* "search-last-claw" () claws))
    ((_ "search-last-claw" first-claws ((exp)))
     (and-let* first-claws exp))        ; (and-let* (... (exp)) )
    ((_ "search-last-claw" first-claws ((var exp)))
     (and-let* first-claws exp))        ; (and-let* (... (var exp)) )
    ((_ "search-last-claw" first-claws (var))
     (and-let* first-claws var))        ; (and-let* (... var) )
    ((_ "search-last-claw" (first-claw ...) (claw . rest))
     (and-let* "search-last-claw" (first-claw ... claw) rest))

    ; now 'body' is present
    ((_ () . body) (begin . body))      ; (and-let* () form ...)
    ((_ ((exp) . claws) . body)         ; (and-let* ( (exp) claw... ) body ...)
     (and exp (and-let* claws . body)))
    ((_ ((var exp) . claws) . body)     ; (and-let* ((var exp) claw...)body...)
     (let ((var exp)) (and var (and-let* claws . body))))
    ((_ (var . claws) . body)           ; (and-let* ( var claw... ) body ...)
     (and var (and-let* claws . body)))
)))

(library (srfi :2) (export and-let*) (import (srfi :2 and-let*)))

