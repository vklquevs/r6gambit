;subset of gambit threads
(library (srfi :21 real-time-multithreading) 
	 (export 
	   ;;threads
	   current-thread make-thread thread?  thread-name thread-specific thread-specific-set!  thread-base-priority thread-base-priority-set! thread-priority-boost thread-priority-boost-set! thread-quantum thread-quantum-set! thread-start!  thread-sleep!  thread-yield!  thread-terminate!  thread-join!
	   ;;mutexes
		 make-mutex mutex?  mutex-name mutex-specific mutex-specific-set!  mutex-state mutex-lock!  mutex-unlock!  
		 ;;condition-variables
		 condition-variable?  make-condition-variable condition-variable-name condition-variable-specific condition-variable-specific-set!  condition-variable-signal!  condition-variable-broadcast!  
		 ;time
		 current-time time? time->seconds seconds->time 
		 ;exceptions and exception handling
		 current-exception-handler with-exception-handler raise join-timeout-exception? abandoned-mutex-exception? terminated-thread-exception? uncaught-exception? uncaught-exception-reason)
	 (import (gambit threads)
		 (gambit time)
		 (gambit exceptions))) 

(library (srfi :21) 
	 (export 
	   ;;threads
	   current-thread make-thread thread?  thread-name thread-specific thread-specific-set!  thread-base-priority thread-base-priority-set! thread-priority-boost thread-priority-boost-set! thread-quantum thread-quantum-set! thread-start!  thread-sleep!  thread-yield!  thread-terminate!  thread-join!
	   ;;mutexes
		 make-mutex mutex?  mutex-name mutex-specific mutex-specific-set!  mutex-state mutex-lock!  mutex-unlock!  
		 ;;condition-variables
		 condition-variable?  make-condition-variable condition-variable-name condition-variable-specific condition-variable-specific-set!  condition-variable-signal!  condition-variable-broadcast!  
		 ;time
		 current-time time? time->seconds seconds->time 
		 ;exceptions and exception handling
		 current-exception-handler with-exception-handler raise join-timeout-exception? abandoned-mutex-exception? terminated-thread-exception? uncaught-exception? uncaught-exception-reason)
	 (import (srfi :21 real-time-multithreading)))
