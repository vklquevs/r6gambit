;srfi-23 in terms of r6rs
(library (srfi :23 error) 
	 (export error)
	 (import (except (rnrs base (6)) error)
		 (rename (rnrs base (6)) (error rnrs:error)))

(define (error message . args)
  (apply rnrs:error #f message args)))

(library (srfi :23) (export error) (import (srfi :23 error)))
