
(library (srfi :27 random-bits) 
	 (export random-integer random-real random-source-make-integers random-source-make-reals random-source-pseudo-randomize!  random-source-randomize!  random-source-state-ref random-source-state-set!  random-source?  make-random-source)
	 (import (primitives random-integer random-real random-source-make-integers random-source-make-reals random-source-pseudo-randomize!  random-source-randomize!  random-source-state-ref random-source-state-set!  random-source?  make-random-source)))

(library (srfi :27) 
	 (export random-integer random-real random-source-make-integers random-source-make-reals random-source-pseudo-randomize!  random-source-randomize!  random-source-state-ref random-source-state-set!  random-source?  make-random-source)
	 (import (srfi :27 random-bits)))
