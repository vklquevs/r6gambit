(library (srfi :39 parameters) 
	 (export make-parameter parameterize)
	 (import
	   (rnrs base)
	   (primitives make-parameter ##parameterize-build))
;(##parameterize param val thunk)
(define-syntax parameterize
  (syntax-rules ()
    ((parametrize bindings body . rest) 
     (##parameterize-build (cons 'parameterize (cons bindings (cons body rest))))))))

(library (srfi :39) (export make-parameter parameterize) (import (srfi :39 parameters)))

