(library (srfi :6 basic-string-ports) 
	 (export get-output-string open-input-string open-output-string)
	 (import (gambit io)))

(library (srfi :6) (export get-output-string open-input-string open-output-string) (import (srfi :6 basic-string-ports)))
