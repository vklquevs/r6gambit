
(library (srfi :8 receive) 
	 (export receive)
	 (import (rnrs base (6)))

  (define-syntax receive
    (syntax-rules ()
		  ((receive formals expression body ...)
		   (call-with-values (lambda () expression)
				     (lambda formals body ...))))))

(library (srfi :8) (export receive) (import (srfi :8 receive)))
