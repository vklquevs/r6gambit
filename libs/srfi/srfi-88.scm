(library (srfi :88)
  (export keyword? keyword->string string->keyword)
  (import (only (gambit extensions) keyword? keyword->string string->keyword)))

