;SRFI-9 in terms of srfi 99 records
(library (srfi :9 records) 
	 (export define-record-type)
	 (import (for (srfi :99 records procedural) expand run)
		 (for (rnrs base) expand run))
	 
(define-syntax define-record-type
  (syntax-rules ()
	((define-record-type type
	     (constructor constructor-tag ...)
	     predicate
	     (field-tag accessor . more) ...)
	 (begin
	   (define type (make-rtd 'type '#((mutable field-tag) ...)))
	   (define constructor (rtd-constructor type '#(constructor-tag ...)))
	   (define predicate (rtd-predicate type))
	   (define-record-field type field-tag accessor . more)
	   ...))))

(define-syntax define-record-field
  (syntax-rules ()
		((define-record-field type field-tag accessor)
		 (define accessor (rtd-accessor type 'field-tag)))
		((define-record-field type field-tag accessor mutator)
		 (begin
		   (define accessor (rtd-accessor type 'field-tag))
		   (define mutator (rtd-mutator type 'field-tag))))))

     
)

(library (srfi :9) (export define-record-type) (import (srfi :9 records)))

