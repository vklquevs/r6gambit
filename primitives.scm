;;;===============================================================================
;;;
;;; Primitives used by various libraries that are not included as part of Gambit library 
;;;
;;; by Arthur T Smyles.
;;;===============================================================================

;R6RS record primitives (slightly more efficient then err5rs for these methods)

(define (gambc-cc type . options )
 (define (join strings)
  (cond
   ((null? strings) "")
   ((null? (cdr strings)) (car strings))
   (else (string-append (car strings) " " (join (cdr strings))))))
 (define (value->string value)
  (cond
   ((list? value) (join value))
   ((symbol? value) (symbol->string value))
   ((string? value) value)
   ((boolean? value) (if value "true" ""))
   (else value)))
  	
  (define (option key default)
   (let get-key ((options options))
    (if (null? options) default
     (if (eq? (car options) key) (cadr options) (get-key (cddr options))))))
 
 (define (envar name key default)
  (string-append (value->string name) "=" (value->string (option key default)) ))

(define (get-environment . vars)
   (filter values
   	(map (lambda (var) (let* ((v (value->string var)) (val (getenv v #f))) (if val (string-append v "=" val) val))) vars)))

   
	(let ((env (append 
			(list 
		    (envar 'GAMBCDIR_INCLUDE include: (path-expand "~~include"))
		    (envar 'BUILD_DYN_OUTPUT_FILENAME output: "")
		    (envar 'BUILD_DYN_CC_OPTIONS cc-options: "")
		    (envar 'BUILD_DYN_LD_OPTIONS_PRELUDE ld-options-prelude: "")
		    (envar 'BUILD_DYN_LD_OPTIONS ld-options: "")
		    (envar 'BUILD_DYN_INPUT_FILENAMES input-files: '())
		    (envar 'GAMBC_CC_VERBOSE verbose: ""))
			(get-environment 'LIBRARY_PATH 'CPATH 'PATH))))
	 (process-status
	  (open-process `(path: ,(path-expand "~~bin/gambc-cc")
			  arguments: ,(list (value->string type))
			  environment: ,env
			  stdout-redirection: #f
			  directory: ,(option directory: #f))))))
	
(define (record-field-mutable? rtd k)
  (if (rtd? rtd)
      ;k doesn't need to be incremented in this case
      (fxbit-set? 1 (vector-ref (##type-fields rtd) (+ (* k 3) 1)))))


(define (record-accessor rtd k)
   (let ((k (+ (##type-field-count (rtd-parent rtd)) k 1)))
     (if (rtd-sealed? rtd)
       (lambda (obj) (##direct-structure-ref obj k rtd #f))
       (lambda (obj) (##structure-ref obj k rtd #f)))))

(define (record-mutator rtd k)
  (if (rtd-field-mutable? rtd k)
    (let ((k (+ (##type-field-count (rtd-parent rtd)) k 1)))
       (if (rtd-sealed? rtd)
	 (lambda (obj value) (##direct-structure-set! obj value k rtd #f))
	 (lambda (obj value) (##structure-set! obj value k rtd #f))))))


;some expander utilities
    (define (read-file fn)
      (let ((p (open-input-file fn)))
        (let f ((x (read p)))
          (if (eof-object? x)
              (begin (close-input-port p) '())
              (cons x
                    (f (read p)))))))

    (define (write-file exps fn)
      (if (file-exists? fn)
          (delete-file fn))
      (let ((p (open-output-file fn)))
        (for-each (lambda (exp)
                    (pretty-print exp p)
                    (newline p))
                  exps)
        (close-output-port p)))

    (define (r6rs#run-r6rs-program filename)
      (r6rs#run-r6rs-sequence (read-file filename)))

    (define (r6rs#expand-file filename target-filename)
      (write-file (r6rs#expand-r6rs-sequence (read-file filename))
                  target-filename))

(define (r6rs#expand-file* target-filename . files)
      (write-file 
	(r6rs#expand-r6rs-sequence 
	  (let loop ((files files))
	    (cond
	      ((pair? files) (append (read-file (car files)) (loop (cdr files))))
	      ((null? files) '()))))
		  target-filename))


(define (r6rs#compile-library library-form output-file . rest)
  (if (null? rest)
     (r6rs#compile-native-library library-form output-file '() '())
	;native files can be either a list, the empty list, or #f which will call the compile-native-library with the empty list
     (r6rs#compile-native-library library-form output-file (or (car rest) '()) (cdr rest))
    ))
  
(define (r6rs#compile-native-library library-form output-file native-files compiler-options)
 (define library-filename (path-strip-directory (path-strip-extension (uri-part output-file 'path)))) 
 (define tmp-dir (string-append "/tmp/" library-filename "-" (number->string (time->seconds (current-time))) "/"))

 (define (get-compiler-option compiler-options key default)
    (cond
	((null? compiler-options) default)
	((equal? (car compiler-options) key) (cadr compiler-options))
	(else (get-compiler-option (cddr compiler-options) key default))))

 (define (compile-files-to-c files)
   (define (file-path f)
	(cond 
	   ((and (uri? f) (or (not (uri-scheme f)) (eq? (uri-scheme f) 'file))) (uri-part f 'path))
	   ((string? f) f)
	   (else (r6rs#assertion-violation 'compile-files-to-c "files must be file uris or strings" files f))))
   (if (list? files)
   	(map (lambda (file)
	      (apply compile-file-to-target (file-path file)  output: tmp-dir compiler-options)) files)
	
	(compile-files-to-c (list files))))
  
 (define (compile-library-to-c library)
  (let ((file (path-expand (string-append library-filename "_.scm") tmp-dir))
	(library-declarations '(declare (standard-bindings)(extended-bindings)(block)))
   ;can't get this to work for some reason. This code is to re-write special forms while evaling the library code since it needs to be evaled incase of other libraries.
   ;in practice this should not cause a problem with non c-* functions.
	(expanded 
		(with-exception-catcher
		 (lambda (e) 
		  (cond ((and (expression-parsing-exception? e) (eq? (expression-parsing-exception-kind e) 'unsupported-special-form)) 
			 (display-exception e)
			 (pp (##source-code (expression-parsing-exception-source e))) 
			 (let* ( (src (##vector-copy (expression-parsing-exception-source e)))
				 (new-src (##vector-set! src  1 (list (##make-source 'list #f) "NAF"))))
			  (pp new-src)
			  (pp (##source-code new-src))
			  (##comp-app ##interaction-cte new-src #f)))
		        (else ((current-exception-handler) e) )))

		  (lambda () (r6rs#expand-r6rs-sequence (list library))))))
	(write-file (cons library-declarations expanded) file)
        (compile-files-to-c file)))

     (create-directory tmp-dir)
	 (parameterize ((current-directory tmp-dir))
	  (let* ((c-files (append (compile-files-to-c native-files) (compile-library-to-c library-form)))
		(input-files (append c-files (list (link-flat (map path-strip-extension c-files) output: (string-append library-filename ".o1.c") warnings?: (get-compiler-option compiler-options warnings?: #f))))))
	   (gambc-cc 'dyn 
	    input-files: input-files   
	    output: (uri-part output-file 'path)
	    cc-options: (get-compiler-option compiler-options cc-options: "")
	    ld-options: (get-compiler-option compiler-options ld-options: "")
	    ld-options-prelude: (get-compiler-option compiler-options ld-options-prelude: "")
	    verbose: (get-compiler-option compiler-options verbose: ""))

	   (if (get-compiler-option compiler-options keep-files: #f)
	    (void) 
	    (begin 
	     (for-each delete-file (directory-files tmp-dir))
	     (delete-directory tmp-dir))))))


;compiles all libraries using bin as the base-uri
;The result is a new catalog with additional entries for every library to have a gambit-binary feature
;if the library has an attribute called native-files, then they are compiled with the library compile-library
(define (r6rs#compile-catalog base-dir catalog bin . compiler-options)	
	(define (library->uri name version base-dir)
	 (define (name->string name delim)
	  (cond
	   ((and (pair? name) (null? (cdr name))) (name->string (car name) ""))
	   ((pair? name) (string-append (name->string (car name) "") delim (name->string (cdr name) delim)))
	   ((number? name) (number->string name))
	   ((symbol? name) (symbol->string name))))

	 (resolve-uri base-dir
          (make-uri #f 
	  (if (pair? version)
	   (string-append (name->string name "-") "-v-" (name->string version "-") ".o1")
	   (string-append (name->string name "-") ".o1")) #f #f)))
   `(catalog 
	,@(map serialize-library (map (lambda (library) 
		(let ((compiled-versions 
		       (map (lambda (version) 
			     (let ((out-file (library->uri (library-name library) (version-number version) bin))
				   (native-files (map (lambda (f) (resolve-uri base-dir (make-uri #f f))) (version-property version 'native-files '()) ))
				   (version-compiler-options (version-property version 'compiler-options '()))
				   (library-form (read-library version)))
			      (r6rs#compile-native-library library-form out-file native-files (append (list keep-files: #t) version-compiler-options compiler-options))

			      (make-version (version-number version) (cons 'gambit-binary (version-requires version)) out-file (append '((dynlib #t)) (version-properties version))))) 
			    (library-versions library))))
		    
		    (make-library (library-name library) (append compiled-versions (library-versions library)) (library-properties library)))) (catalog->libraries base-dir catalog)))))

    (define (r6rs#expand-r5rs-file filename target-filename r6rs-env)
      (write-file (r6rs#expand-r5rs-sequence (read-file filename) r6rs-env) target-filename))


