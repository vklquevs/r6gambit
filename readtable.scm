;;; sets readtable to r6rs mode
;primitives to access readtable macros
(include "~~lib/_gambit#.scm")

(##define-macro (macro-read-next-char-or-eof re) ;; possibly returns end-of-file
  `(macro-read-char (macro-readenv-port ,re)))


(define make-r6rs-readtable
   (let ((readtable-type (##structure-type (current-readtable))))
	(define readtable-sharp-bang-table (rtd-accessor readtable-type 'sharp-bang-table))
	(define readtable-sharp-bang-table-set! (rtd-mutator readtable-type 'sharp-bang-table))
        ;identical to default except none-marker are treated as comment
	(define (read-sharp-bang re next start-pos)
	 (let ((old-pos (macro-readenv-filepos re)))
	  (macro-read-next-char-or-eof re) ;; skip char after #\#
	  (if (macro-readenv-allow-script? re)
	   (##script-marker)
	   (begin
	    (macro-readenv-filepos-set! re start-pos) ;; set pos to start of datum
	    (let ((name (##build-delimited-string re #\space 0)))
	     (let ((x
		    (##read-assoc-string=?
		     re
		     name
		     (macro-readtable-sharp-bang-table
		      (macro-readenv-readtable re)))))
	      (cond
                 ((and x (eq? (cdr x) (##none-marker))) (##read-datum-or-label-or-none-or-dot re)) 
	      	 ((x) (macro-readenv-wrap re (cdr x)))
		 (else
		   (##raise-datum-parsing-exception 'invalid-sharp-bang-name re name)
		   (macro-readenv-filepos-set! re old-pos) ;; restore pos
		   (##read-datum-or-label-or-none-or-dot re))))))))) ;; skip error

	(lambda ()
	    (let ((result (##make-standard-readtable)))
		(readtable-sharp-bang-table-set! result (cons `("r6rs" . ,(##none-marker)) ##standard-sharp-bang-table))
		    
                    (##readtable-char-sharp-handler-set! result #\! read-sharp-bang)
                     
		result))))
	
