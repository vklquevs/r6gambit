;;; 
;;; Runtime include file:
;;; Contains the minimal set of binding necessary
;;; for running a fully expanded program.
;;;

(define r6rs#unspecified (if #f #f))
(define r6rs#library (make-rtd 'library '#((immutable name) (immutable envs) (immutable exports) (immutable imports) (immutable builds) (immutable visiter) (immutable invoker) (immutable build) (mutable visited?) (mutable invoked?))))

(define r6rs#make-library 
   (let ((constructor (rtd-constructor r6rs#library)))
	(lambda (name envs exports imports builds visiter invoker build)
	   (constructor name envs exports imports builds visiter invoker build #f #f))))

;(define (r6rs#make-library name envs exports imports builds visiter invoker build)
;  (list name envs exports imports builds visiter invoker build))

(define r6rs#library-name (rtd-accessor r6rs#library 'name))
(define r6rs#library-envs (rtd-accessor r6rs#library 'envs))
(define r6rs#library-exports (rtd-accessor r6rs#library 'exports))
(define r6rs#library-imports (rtd-accessor r6rs#library 'imports))
(define r6rs#library-builds  (rtd-accessor r6rs#library 'builds))
(define r6rs#library-visiter (rtd-accessor r6rs#library 'visiter))
(define r6rs#library-invoker (rtd-accessor r6rs#library 'invoker))
(define r6rs#library-build   (rtd-accessor r6rs#library 'build))
(define r6rs#library-visited? (rtd-accessor r6rs#library 'visited?))
(define r6rs#library-invoked? (rtd-accessor r6rs#library 'invoked?))

(define r6rs#library-visited?-set! (rtd-mutator r6rs#library 'visited?))
(define r6rs#library-invoked?-set! (rtd-mutator r6rs#library 'invoked?))

(define r6rs#imported '())
(define (r6rs#import-libraries-for imports builds phase importer run-or-expand)
  (define (import-libraries imports builds phase)
    (for-each (lambda (import build)
                (let ((name   (car import))
                      (levels (cdr import)))
                  (for-each (lambda (level)
                              (import-library name build (+ phase level)))
                            levels)))
              imports
              builds)
    (values))
  (define (import-library name build phase)
    (if (not (member (cons name (cons phase run-or-expand)) r6rs#imported))
        (let ((library (r6rs#lookup-library name)))
          (or (not build)
              (eq? build (r6rs#library-build library))
              (r6rs#assertion-violation 'import "Client was expanded against a different build of this library" name))
          (import-libraries (r6rs#library-imports library) 
                            (r6rs#library-builds library)
                            phase)
          (importer library phase r6rs#imported)
          (set! r6rs#imported (cons (cons name (cons phase run-or-expand)) r6rs#imported)))))
  (import-libraries imports builds phase))

(define (r6rs#import-libraries-for-run imports builds phase)
  (r6rs#import-libraries-for imports 
                           builds
                           phase 
                           (lambda (library phase imported)
                             (if (and (= phase 0)
                                      (not (r6rs#library-invoked? library)))
                                 (begin 
                                   ((r6rs#library-invoker library))
                                   (r6rs#library-invoked?-set! library #t))))
                           'run))



(define r6rs#register-library! #f)
(define r6rs#lookup-library    #f)
(define r6rs#register-catalogs! #f)
(define r6rs#catalog #f)
(let ((table '())
      (catalog (make-hash-table)))

     (define (r6rs#load-library name)
	(let ((version (find-version catalog name (lambda rest #t) '(gambit-binary gambit))))
		(cond 
		    ((and version (version-property version 'dynlib)) (load (uri-part (version-uri version) 'path)))
		    ((version? version) 
			(parameterize ((current-readtable (make-r6rs-readtable))) 
				(r6rs#run-r6rs-sequence (list (read-library version)))))
		    (else (r6rs#assertion-violation 'lookup-library "Library not registered in catalog!" name (hash-table->alist catalog))))
		(r6rs#lookup-library name)))


      #;(let ((library (hash-table-ref/default catalog name #f)))
       (if library
	(let* ((versions (library-versions library))
		
	       ;first try to find the gambit-binary and load that
	       (gambit-binary (call/cc (lambda (return)
					(for-each 
					 (lambda (version) (if (memq 'gambit-binary (version-features version)) (return version)))
					 versions) #f)))
		(source (or gambit-binary (r6rs#read-library (call/cc (lambda (return)
								  (for-each 
								   (lambda (version) 
								    (if (not (memq 'gambit-binary (version-features version))) (return version)))
								   versions)))))))

	(if gambit-binary 
	     (load (version-uri gambit-binary))
	     (r6rs#run-r6rs-sequence (list source))) ;no gambit binary; load first source file
	  (r6rs#lookup-library name))
	(r6rs#assertion-violation 'lookup-library "Library not registered in catalog!" name (hash-table->alist catalog))))      
			

     (define version-reference->predicate
      (let ()
       (define (and? v->p references) 
	(let ((predicates (map v->p references)))
	 (lambda (v) 
	  (call/cc (lambda (return)
		    (for-each (lambda (p) (if (not (p v)) (return #f))) predicates)
		    (return #t))))))

       (define (or? v->p references)
	(let ((predicates (map v->p references)))
	 (lambda (v)
	  (call/cc (lambda (return)
		    (for-each (lambda (p) (if (p v) (return #t))) predicates)
		    (return #f))))))

       (define (not? v->p reference)
	(let ((predicate (v->p reference)))
	 (lambda (v) (not (predicate v)))))

       (define (subversion-predicate subversion)
	(if (integer? subversion) (lambda (sv) (eqv? subversion sv))
	 (case (car subversion)
	  ((>=) (lambda (sv) (>= sv (cadr subversion))))
	  ((<=) (lambda (sv) (<= sv (cadr subversion))))
	  ((and) (and? subversion-predicate (cdr subversion)))
	  ((or) (or? subversion-predicate (cdr subversion)))
	  ((not) (not? subversion-predicate (cadr subversion))))))

	(define (compare? version predicates)
	 (call/cc (lambda (return)
		   (for-each (lambda (p sv) 
			      (if p (return #t)
			       (if (not (p sv)) (return #f))) predicates version))
		   (return #t))))

	(define (make-list k fill) (if (zero? k) '() (cons fill (make-list (- k 1) fill))))

	(lambda (version-reference)
	 (if (null? version-reference) (lambda (v) #t)
	  (case (car version-reference)
	   ((and) (and? version-predicate (cdr version-reference)))
	   ((or) (or? version-predicate (cdr version-reference)))
	   ((not) (not? version-predicate (cadr version-reference)))
	   (else (let* ((sv-predicates (map subversion-predicate version-reference))
			(sv-len (length sv-predicates)))
		  (lambda (v)
		   (cond 
		    ((= sv-len (length v)) (compare? v sv-predicates)) 
		    ((< sv-len (length v)) (compare? v (append sv-predicates (make-list (- (length v) sv-len) #t))))
		    (else #f))))))))))

  (set! r6rs#register-catalogs! (lambda (catalogs)
	(for-each (lambda (library) (hash-table-set! catalog (library-name library) library)) 
		  (apply append (map catalog->libraries catalogs)))
  (set! r6rs#catalog (lambda () (pp (hash-table->alist catalog))))
))

  (set! r6rs#register-library! 
        (lambda (library)
          (set! table (cons (cons (r6rs#library-name library) library) table))
	  (set! r6rs#imported (filter (lambda (entry)
                                      (not (equal? (r6rs#library-name library) 
                                                   (car entry))))
                                    r6rs#imported))))

  (set! r6rs#lookup-library 
        (lambda (name)
          (let ((library (assoc name table)))
            (if library
                (cdr library)
		(begin
		   (r6rs#load-library name)
		   ;(r6rs#lookup-library name)
))))))


;; Only instantiate part of the bootstrap library 
;; that would be needed for invocation at runtime.

(r6rs#register-library! 
 (let ((error (lambda () 
                (r6rs#assertion-violation 
                 'runtime.scm
                 "Attempt to use runtime instance of (core primitive-macros) for expansion.  Make sure expander.scm is loaded after runtime.scm."))))
   (r6rs#make-library
    '(core primitive-macros)
    ;; envs
    error
    ;; exports
    '()
    ;; imported-libraries
    '()
    ;; builds
    '()
    ;; visiter
    error
    ;; invoker
    (lambda () (values))
    ;; build
    'system)))
