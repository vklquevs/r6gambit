;; Implementation of srfi-66
;; Since gambit provides most of the functions, only providing what is not allready done. r6gambit expects all of these. There is an extension function (native-endiannes) which should provide the endianness of the platform

;(u8vector? obj)
;(make-u8vector k fill)
;(u8vector octet ...)
;(u8vector->list u8vector)
;(list->u8vector octets)
;(u8vector-length u8vector)
;(u8vector-ref u8vector k)
;(u8vector-set! u8vector k octet)
;(u8vector-copy u8vector)

(define (u8vector-compare u8vector-1 u8vector-2)
  (cond
	((< (u8vector-length u8vector-1) (u8vector-length u8vector-2)) -1)
	((> (u8vector-length u8vector-1) (u8vector-length u8vector-2)) 1)
	(else (let compare ((i 0))
		(cond
		   ((= i (u8vector-length u8vector-1)) 0)
		   ((< (u8vector-ref u8vector-1 i) (u8vector-ref u8vector-2 i)) -1)
		   ((> (u8vector-ref u8vector-1 i) (u8vector-ref u8vector-2 i)) 1)
		   (else (compare (+ i 1))))))))

(define (u8vector=? u8vector-1 u8vector-2) (= (u8vector-compare u8vector-1 u8vector-2) 0))

(define (u8vector-copy! source source-start target target-start n)
  (subu8vector-move! source source-start (+ source-start n) target target-start))


(define native-endianness
   (let ((ne (c-lambda () bool "
	#ifdef ___BIG_ENDIAN 
	   ___result=1; 
	#else 
	   ___result=0; 
	#endif")))

	(lambda () (if (ne) 'big 'little))))

  

