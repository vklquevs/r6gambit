;;Type constructors and predicate
;;    hash-table-type, make-hash-table, hash-table?, alist->hash-table 
;;Reflective queries
;;    hash-table-equivalence-function, hash-table-hash-function, hash-table-mutable?
;;Dealing with single elements
;;    hash-table-ref, hash-table-ref/default, hash-table-set!, hash-table-delete!, hash-table-exists?, hash-table-update!, hash-table-update!/default 
;;Dealing with the whole contents
;;    hash-table-size, hash-table-keys, hash-table-values, hash-table-walk, hash-table-fold, hash-table->alist, hash-table-copy, hash-table-merge! hash-table-clear! 
;;Hashing
;;    hash, string-hash, string-ci-hash, hash-by-identity 
;;
;; implementation requires srfi-99
;primitives to access gcht in gambit
(include "~~lib/_gambit#.scm")

;Constructors
(define hash-table-type (##structure-type (make-table)))

(define (make-hash-table . rest)
  (let ((equal? (if (null? rest) #f (car rest)))
	(hash (if (or (null? rest) (null? (cdr rest))) #f (cadr  rest)))
	(args (if (or (null? rest) (null? (cdr rest))) '() (cddr rest))))
   (cond
	((and equal? hash) (apply make-table test: equal? hash: hash args))
	(equal? (make-table test: equal?))
	(else (make-table)))))

(define hash-table? table?)

(define (alist->hash-table alist . rest)
  (let ((equal? (if (null? rest) #f (car rest)))
	(hash (if (or (null? rest) (null? (cdr rest))) #f (cadr  rest)))
	(args (if (or (null? rest) (null? (cdr rest))) '() (cddr rest))))
   (cond
	((and equal? hash) (apply list->table alist test: equal? hash: hash args))
	(equal? (list->table alist test: equal?))
	(else (list->table alist)))))

;reflection

(define (hash-table-equivalence-function hash-table) 
	(macro-table-test hash-table))
 ;(rtd-accessor hash-table-type 'test))
(define (hash-table-hash-function hash-table)
	(macro-table-hash hash-table))
; (rtd-accessor hash-table-type 'hash))


(define (hash-table-mutable? hash-table) (not (bit-set? 6 (macro-table-flags hash-table))))
(define (hash-table-mutable?-set! hash-table mutable?)
  (macro-table-flags-set! hash-table (bitwise-merge 64 (macro-table-flags hash-table) (if mutable? 0 64))))

;dealing with single elements

(define (hash-table-ref hash-table key . rest)
 (let ((thunk (if (pair? rest) (car rest) #f)))
 (##table-access hash-table key
                 (lambda (table key gcht probe2 default-value) 
			(macro-gc-hash-table-val-ref gcht probe2))
 		 (lambda (table key gcht probe2 deleted2 default-value)
			(if default-value (default-value)
				(##raise-unbound-table-key-exception hash-table-ref table key))) thunk))) 

(define (hash-table-ref/default hash-table key default) 
	(table-ref hash-table key default))


(define (hash-table-set! hash-table key value)
   (cond 
	((not (hash-table? hash-table)) (r6rs#assertion-violation 'hash-table-set! "First argument must be a hash-table" hash-table))
	((hash-table-mutable? hash-table) (table-set! hash-table key value))))

(define (hash-table-delete! hash-table key)
  (if (hash-table-mutable? hash-table)
	(table-set! hash-table key)))

(define (hash-table-exists? hash-table key)
	(##table-access hash-table key (lambda rest #t) (lambda rest #f) #!void))

(define (hash-table-update! hash-table key function . rest)
  (if (hash-table-mutable? hash-table)
  (let ((thunk (if (pair? rest) (car rest) #f)))
   (##table-access
       hash-table
       key
       (lambda (table key gcht probe2 val)
             (macro-gc-hash-table-val-set! gcht probe2 (function (macro-gc-hash-table-val-ref gcht probe2)))
             (##void))
       (lambda (table key gcht probe2 deleted2 val)
	;; key was not found (search ended at position "probe2" and the
		;; first deleted entry encountered is at position "deleted2")
	(if val 
	 (if deleted2
	  (let ((count (##fixnum.+ (macro-gc-hash-table-count gcht) 1)))
	   (macro-gc-hash-table-count-set! gcht count)
	   (macro-gc-hash-table-key-set! gcht deleted2 key)
	   (macro-gc-hash-table-val-set! gcht deleted2 (function (val)))
	   (##void))
	  (let ((count (##fixnum.+ (macro-gc-hash-table-count gcht) 1))
		(free (##fixnum.- (macro-gc-hash-table-free gcht) 1)))
	   (macro-gc-hash-table-count-set! gcht count)
	   (macro-gc-hash-table-free-set! gcht free)
	   (macro-gc-hash-table-key-set! gcht probe2 key)
	   (macro-gc-hash-table-val-set! gcht probe2 (function (val)))
	   (if (##fixnum.< free 0)
	    (##table-resize! table)
	    (##void))))
	 (##raise-unbound-table-key-exception hash-table-ref table key)))
	thunk
      ))))

(define (hash-table-update!/default hash-table key function default)
  (hash-table-update! hash-table key function (lambda () default)))

;Dealing with the whole contents
(define hash-table-size table-length)
(define (hash-table-keys hash-table)
  (##table-foldl (lambda (base key) (cons key base)) '() (lambda (key value) key) hash-table))

(define (hash-table-values hash-table)
   (##table-foldl (lambda (base value) (cons value base)) '() (lambda (key value) value) hash-table))

(define (hash-table-walk table proc)
	(cond
	   ((not (hash-table? table)) (r6rs#assertion-violation 'hash-table-walk "first argument must be a table" table))
	   ((not (procedure? proc)) (r6rs#assertion-violation 'hash-table-walk "second argument must be a procedure" proc))
	   (else  (table-for-each proc table))))

(define (hash-table-fold hash-table f init-value)
   (##table-foldl (lambda (base key-value)
			(f (car key-value) (cdr key-value) base))
		  init-value
		  cons
		  hash-table))


(define hash-table->alist table->list)
(define hash-table-copy table-copy)
		
(define (hash-table-merge! hash-table-1 hash-table-2)
  (if (hash-table-mutable? hash-table-1) 
	(table-merge! hash-table-1 hash-table-2 #t)
	(table-merge hash-table-1 hash-table-2 #t)))

(define (hash-table-clear! hash-table . rest)
  (if (hash-table-mutable? hash-table)
  (let ((size (if (pair? rest) (car rest) 0)))
	(macro-table-gcht-set! hash-table size))))

;Hashing
(define hash equal?-hash)
(define string-hash string=?-hash)
(define string-ci-hash string-ci=?-hash)
(define hash-by-identity eq?-hash)

	
	
