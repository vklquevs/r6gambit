;;;===============================================================================
;;;
;;; Gambit srfi-99 records procedural and inspection implementation (with r6rs optional extensions).
;;; For this library to function, you must implement assertion-violation function with parameters
;;; defined in r6rs.
;;;
; Copyright (c) 2008, Arthur T Smyles
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of the <organization> nor the
;       names of its contributors may be used to endorse or promote products
;       derived from this software without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY Arthur T Smyles ''AS IS'' AND ANY
; EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL Arthur T Smyles BE LIABLE FOR ANY
; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;;===============================================================================

(define (make-rtd name fieldspecs . rest)
  (define parent (and (pair? rest) (car rest)))
  (define sealed? (and (pair? rest) (memq 'sealed rest) #t))
  (define opaque? (and (pair? rest) (memq 'opaque rest) #t))
  (define uid (cond ((and (pair? rest) (memq 'uid rest)) => cadr) (else #f)))
  (define (parse-fields fields)
    (define (make-field-flags mutable? printable? extensional? init?)
      (+ (if printable? 0 1)
	 (if mutable? 0 2)
	 (if extensional? 0 4)
	 (if init? 0 8)))
    (let* ((field-count (vector-length fields))
	  (result (make-vector (* field-count 3))))
      (let process-fields ((i 0))
	    (if (not (< i field-count)) result
	    ;gambit has other attributes for fields such as if they are printable and it's contribution to equality
	    (let* ((field (vector-ref fields i))
		   (printable? (or (symbol? field) (null? (cddr field)) (and (memq 'printable (cddr field)) #t) (and (memq 'unprintable (cddr field)) #f)))
		   (extensional? (or (symbol? field) (null? (cddr field)) (and (memq 'extensional (cddr field)) #t) (and (memq 'intensional (cddr field)) #f)))
		   (init (and (not (or (symbol? field) (null? (cddr field)))) 
			(let process-init ((lst (cddr field)))
				(cond ((null? lst) #f)
				      ((and (pair? (car lst)) (eq? 'init (caar lst))) (cdar lst))
				      (else (process-init (cdr lst)))))))
				
		  (flags (make-field-flags (or (symbol? field) (eq? 'mutable (car field))) printable? extensional? init)) 
		  (field-name (if (symbol? field) field (cadr field)))
		  (j (* i 3)))
	      (vector-set! result j field-name)
	      (vector-set! result (+ j 1) flags)
	      (vector-set! result (+ j 2) init); used for setting initial value
	      (process-fields (+ i 1)))))))
   (cond
	((and parent (not (rtd? parent))) (r6rs#assertion-violation 'make-rtd "Third argument must be an rtd or #f" parent)))
   (let* ((flags (##fixnum.+ (if opaque? 1 0) (if sealed? 0 2) (if uid 16 0)))
	  (uid (if uid uid (make-uninterned-symbol (symbol->string name)))))

     (##structure ##type-type uid name flags parent (parse-fields fieldspecs))))


(define rtd? ##type?)

(define (rtd-constructor rtd . rest) 
      ; Computes permutation and allocates permutation buffer
      ; when the constructor is created, not when the constructor
      ; is called.  More error checking is recommended.

      (define (make-constructor fieldspecs allnames maker)
        (let* ((k (length fieldspecs))
               (n (length allnames))
               (buffer (make-vector n (void)))
               (reverse-all-names (reverse allnames)))

          (define (position fieldname)
            (let ((names (memq fieldname reverse-all-names)))
              ;(assert names)
              (- (length names) 1)))

          (let ((indexes (map position fieldspecs)))

            ; The following can be made quite efficient by
            ; hand-coding it in some lower-level language,
            ; e.g. Larceny's mal.  Even case-lambda would
            ; be good enough in most systems.

            (lambda args
              ;(assert (= (length args) k))
              (for-each (lambda (arg posn)
                          (vector-set! buffer posn arg))
                        args indexes)
              (apply maker (vector->list buffer))))))
 
  (if (null? rest) (lambda fields (apply ##structure rtd fields))
    (begin ;(assert (null? (cdr rest)))
    	(make-constructor
            (vector->list (car rest))
            (vector->list (rtd-all-field-names rtd))
            (rtd-constructor rtd)))))

(define (rtd-predicate rtd)
  (let ((uid (rtd-uid rtd)))
  (if (rtd-sealed? rtd)
    (lambda (obj) (##structure-direct-instance-of? obj uid))
    (lambda (obj) (##structure-instance-of? obj uid)))))

;this is an extension function. Creates a generic destructor
(define (rtd-destructor rtd . predicate)
  (if (rtd? rtd)
  (let ((predicate? (if (null? predicate) (rtd-predicate rtd) (car predicate)))
	(rtd-indexer (eval `(lambda (rtd field)
		(case field
			,@(reverse (rtd-map-all rtd (lambda (i name . rest) `((,name) ,(+ i 1)))))
			(else (r6rs#assertion-violation (rtd-name rtd) "Invalid field!" field))))))
	(accessor (if (rtd-sealed? rtd) ##direct-structure-ref ##structure-ref))
	(decons (lambda (obj) (let ((result (##subvector obj 1 (##vector-length obj)))) (##subtype-set! result 5 #|boxvalues type|#) result))))
  (lambda (obj . fields)
	(cond
	    ((not (predicate? obj))(r6rs#assertion-violation 'rtd-deconstructor (string-append "First argument must be of type " (symbol->string (rtd-name rtd))) obj))
	    ((null? fields) (decons obj)) ;(apply values (rtd-map-all rtd (lambda (i . rest) (accessor obj (+ i 1) rtd #f)))))
	    ((null? (cdr fields)) (values (accessor obj (rtd-indexer rtd (car fields)) rtd #f)))
	    (else (apply values (map (lambda (field) (accessor obj (rtd-indexer rtd field) rtd #f)) fields))))))
  (r6rs#assertion-violation 'rtd-deconstructor "first argument must be a record type descriptor" rtd)))
		

(define (rtd-accessor rtd field)
  (if (rtd? rtd)
      (call/cc (lambda (return)
	(rtd-for-each rtd 
		      (lambda (i name . rest)
			(if (eq? field name)
			  (let ((index (+ i 1)))
			    (if (rtd-sealed? rtd)
			      (return (lambda (obj) (##direct-structure-ref obj index rtd #f)))
			      (return (lambda (obj) (##structure-ref obj index rtd #f))))))))
	(if (rtd-parent rtd) (return (rtd-accessor (rtd-parent rtd) field))
	   (r6rs#assertion-violation 'rtd-accessor "invalid field name" field))))
    (r6rs#assertion-violation 'rtd-accessor "first argument must be a record type descriptor" rtd)))

(define (rtd-mutator rtd field)
  (if (rtd? rtd)
    (call/cc
      (lambda (return)
	(rtd-for-each rtd
          (lambda (i name flags init)
	    (if (and (eq? name field) (not (fxbit-set? 1 flags))) ;checks if the field is mutable
	      (let ((index (+ i 1)))
	      (if (rtd-sealed? rtd)
		(return (lambda (obj value) (##direct-structure-set! obj value index rtd #f)))
		(return (lambda (obj value) (##structure-set! obj value index rtd #f))))))))
	(if (rtd-parent rtd) (return (rtd-mutator (rtd-parent rtd) field)) 
	  (r6rs#assertion-violation 'rtd-mutator "invalid field" field))))
    (r6rs#assertion-violation 'rtd-accessor "first argument must be an record type descriptor" rtd)))

(define (record? obj)
  (and (##structure? obj) (not (rtd-opaque? (##structure-type obj)))))

(define (record-rtd obj)
  (if (record? obj) (##structure-type obj)
    (r6rs#assertion-violation 'record-rtd "first argument must of type record" obj) 
    ))

(define (rtd-name rtd) (if (rtd? rtd) (##type-name rtd)))
(define (rtd-parent rtd) (if (rtd? rtd) (##type-super rtd)))
;ERR5RS standard extensions
(define (rtd-uid rtd) (if (rtd? rtd) (##type-id rtd)))
(define (rtd-sealed? rtd) (if (rtd? rtd) (not (fxbit-set? 1 (##type-flags rtd)))))
(define (rtd-opaque? rtd) (if (rtd? rtd) (fxbit-set? 0 (##type-flags rtd))))
(define (rtd-generative? rtd) (if (rtd? rtd) (not (fxbit-set? 4 (##type-flags rtd)))))


; kons is a procedure (accum index name flags init-value)
(define (rtd-fold-left rtd kons knil)
   (define fields (##type-fields rtd))
   (define offset (##type-field-count (rtd-parent rtd)))
   (let process-field ((i 0)
		       (l (vector-length fields))
		       (accum knil))
      (if (< i l) 
	(process-field (+ i 3) l (kons accum (+ offset (/ i 3)) (vector-ref fields i) (vector-ref fields (+ i 1)) (vector-ref fields (+ i 2))))
	accum)))
 
(define (rtd-fold-right rtd kons knil)
   (define fields (##type-fields rtd))
   (define offset (##type-field-count (rtd-parent rtd)))
   (let process-field ((i (- (vector-length fields) 3))
		       (l 0)
		       (accum knil))
      (if (>= i l) 
	(process-field (- i 3) l (kons accum (+ offset (/ i 3)) (vector-ref fields i) (vector-ref fields (+ i 1)) (vector-ref fields (+ i 2))))
	accum)))

(define (rtd-fold-all-left rtd kons knil)
	(if (rtd-parent rtd)
	    (rtd-fold-left rtd kons (rtd-fold-left (rtd-parent rtd) kons knil))
	    (rtd-fold-left rtd kons knil)))

(define (rtd-fold-all-right rtd kons knil)
	(if (rtd-parent rtd)
		(rtd-fold-right (rtd-parent rtd) kons (rtd-fold-right rtd kons knil))
		(rtd-fold-right rtd kons knil)))

(define (rtd-map rtd proc . rest)
	(define direction (or (null? rest) (car rest)))
	(define kons (lambda (accum . rest) (cons (apply proc rest) accum)))
	(if direction
	      (rtd-fold-right rtd kons '())
	      (rtd-fold-left rtd kons '())))

(define (rtd-for-each rtd proc . rest)
	(define direction (or (null? rest) (car rest)))
	(define kons (lambda (accum . rest) (apply proc rest)))
	(if direction
	      (rtd-fold-right rtd kons '())
	      (rtd-fold-left rtd kons '()))
	(values))

(define (rtd-map-all rtd proc . rest)
	(define direction (or (null? rest) (car rest)))
	(define kons (lambda (accum . rest) (cons (apply proc rest) accum)))
	(if direction
	      (rtd-fold-all-right rtd kons '())
	      (rtd-fold-all-left rtd kons '())))

(define (rtd-for-all rtd proc . rest)
	(define direction (or (null? rest) (car rest)))
	(define kons (lambda (accum . rest) (apply proc rest)))
	(if direction
	      (rtd-fold-all-right rtd kons '())
	      (rtd-fold-all-left rtd kons '()))
	(values))


	 
(define (rtd-field-names rtd)
  (list->vector (rtd-map rtd (lambda (i name . rest) name))))
(define (rtd-all-field-names rtd)
  (list->vector (rtd-map-all rtd (lambda (i name . rest) name))))

(define (rtd-field-flag-procedure flag)
  (lambda (rtd field)
  (if (rtd? rtd)
    (call/cc (lambda (k)
    	(rtd-for-each rtd
		      (lambda (i name flags init)
			(if (eq? name field) (k (not (fxbit-set? flag flags)))))))))))

(define rtd-field-printable? (rtd-field-flag-procedure 0))
(define rtd-field-mutable? (rtd-field-flag-procedure 1))
(define rtd-field-extensional? (rtd-field-flag-procedure 2))
(define rtd-field-init? (rtd-field-flag-procedure 3))

