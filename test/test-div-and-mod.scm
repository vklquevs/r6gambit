#!/usr/bin/env gsi 
; vim: syntax=scheme
(r6rs "~/.gambit/catalog.scat")

(ex:repl '(
  (program
      (import  (rnrs base)  (gambit debug)) 
       (begin	
	(pp (div 123 10))
	(pp (mod 123 10))
	(pp (div 123 -10))
	(pp (mod 123 -10))
	(pp (div -123 10))
	(pp (mod -123 10))
	(pp (div -123 -10))
	(pp (mod -123 -10)))
	(begin	
	(pp (div0 123 10))
	(pp (mod0 123 10))
	(pp (div0 123 -10))
	(pp (mod0 123 -10))
	(pp (div0 -123 10))
	(pp (mod0 -123 10))
	(pp (div0 -123 -10))
	(pp (mod0 -123 -10))))))
