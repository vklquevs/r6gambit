;requires srfi-99, srfi-69, error.scm
(define $uri (make-rtd 'uri '#(
	(immutable scheme) ; #f or a symbol
	(immutable body) ; a string
	(immutable query) ; a string (may be empty), or #f if not specified 
	(immutable fragment); a string (may be empty), or #f if not specified
)))

(define uri? (rtd-predicate $uri))
(define uri-scheme (rtd-accessor $uri 'scheme))
(define uri-body (rtd-accessor $uri 'body))
(define uri-query (rtd-accessor $uri 'query))
(define uri-fragment (rtd-accessor $uri 'fragment))

(define make-uri 
  (let* ((constructor (rtd-constructor $uri))
	 (econstructor (lambda (scheme body query fragment)
	(cond
	    ((not (or (symbol? scheme) (not scheme))) (r6rs#assertion-violation 'make-uri "uri scheme must be a symbol of #f" scheme))
      	    ((not (string? body)) (r6rs#assertion-violation 'make-uri "uri body must be a string" body))
      	    ((not (or (string? query) (boolean? query))) (r6rs#assertion-violation 'make-uri "uri query must be a string or boolean"))
            ((not (or (string? fragment) (boolean? fragment))) (r6rs#assertion-violation 'make-uri "uri fragment must be a string or boolean"))
            (else (constructor scheme body query fragment))))))
	(lambda (scheme body . rest)
	   (cond
	       ((null? rest) (econstructor scheme body #f #f))
	       ((and (pair? rest) (null? (cdr rest)))  (econstructor scheme body (car rest) #f))
	       ((and (pair? rest) (pair? (cdr rest)) (null? (cddr rest))) (econstructor scheme body (car rest) (cadr rest)))
	       (else (r6rs#assertion-violation 'make-uri "this function only accepts 4 arguments"))))))

(define (uri uri-string)
  (define (empty-string? s) (equal? s ""))
  (define (index-of s c)
		(let loop ((i 0))
		   (cond 
			((= i (string-length s)) #f)
		   	((char=? (string-ref s i) c) i)
			(else (loop (+ i 1))))))
	(let* ((indexes (map (lambda (c) (index-of uri-string c)) '(#\: #\? #\#))) 
		(scheme (and (car indexes) (substring uri-string 0 (car indexes))))
		(scheme? (and scheme (or (not (index-of scheme #\/)) #f))) ; check that the scheme only has alpha+digit+-._
		(fragment (and (caddr indexes) (substring uri-string (+ (caddr indexes) 1) (string-length uri-string))))
	      	(query (and (cadr indexes) (substring uri-string (+ (cadr indexes) 1) (or (caddr indexes) (string-length uri-string)))))
		(body (substring uri-string (+ (or (car indexes) -1) 1) (or (cadr indexes) (caddr indexes) (string-length uri-string)))))
	  (make-uri (and scheme? (string->symbol scheme)) 
		    (cond
			(scheme? body)
			((not scheme) body)
			(else (string-append scheme ":" body)))
		     query fragment)))  

		
(define (uri->string uri) (string-append 
	(if (uri-scheme uri) (string-append (symbol->string (uri-scheme uri)) ":") "") 
	(uri-body uri) 
	(if (uri-query uri) (string-append "?" (uri-query uri)) "")
	(if (uri-fragment uri) (string-append "#" (uri-fragment uri)) "")))

(define (resolve-uri base . relatives)
   (define (last-index-of s c)
		(let loop ((i (- (string-length s) 1)))
		   (cond 
			((< i 0) #f)
		   	((char=? (string-ref s i) c) i)
			(else (loop (- i 1))))))

   (define (merge-path base uri)
	(if (and (uri-part? base 'authority) (not (uri-part? base 'path)))
	   (string-append "/" (uri-part uri 'path))
	   (string-append (substring (uri-body base) 0 (+ (last-index-of (uri-body base) #\/) 1)) (uri-part uri 'path))))

   (define (remove-dot-segments path)
      (define (next-index s c i)
	(let loop ((i i))
	    (cond
		((= i (string-length s)) #f)
		((char=? (string-ref s i) c) i)
		(else (loop (+ i 1))))))
	(let loop ((buffer "")
		   (cursor 0))
	   (if (>= cursor (string-length path)) buffer
            (cond
		((string=? (substring path cursor (min (string-length path) (+ cursor 3))) "../") 
			(loop buffer (+ cursor 4) ))
		((string=? (substring path cursor (min (string-length path) (+ cursor 2))) "./") 
			(loop buffer (+ cursor 1)));keep /
		((string=? (substring path cursor (min (string-length path) (+ cursor 3))) "/./")
			(loop buffer (+ cursor 2))); keep /
		((string=? (substring path cursor (string-length path)) "/.")
			(loop buffer (+ cursor 2)))
		((string=? (substring path cursor (min (string-length path) (+ cursor 4))) "/../")
			(loop (substring buffer 0 (last-index-of buffer #\/)) (+ cursor 4)))
		((string=? (substring path cursor (string-length path)) "/..")
			(loop (substring buffer 0 (last-index-of buffer #\/)) (+ cursor 4)))
		((string=? (substring path cursor (string-length path)) "..")
			(loop buffer (+ cursor 3)))
		((string=? (substring path cursor (string-length path)) ".")
			(loop buffer (+ cursor 2)))
		(else 
			(let ((new-cursor (or (next-index path #\/ (+ cursor 1)) (string-length path))))
			     (loop (string-append buffer (substring path cursor new-cursor)) new-cursor)))))))
    
   (define (resolve base uri)
	(cond 
	  ((uri-scheme uri) (make-uri (uri-scheme uri) (string-append "//" (or (uri-part uri 'authority) "") (remove-dot-segments (uri-part uri 'path))) (uri-query uri) (uri-fragment uri)))
	  ((uri-part uri 'authority) (make-uri (uri-scheme base) (string-append "//" (uri-part uri 'authority) (remove-dot-segments (uri-part uri 'path))) (uri-query uri) (uri-fragment uri)))
	  ((not (uri-part uri 'path)) (make-uri (uri-scheme base) (uri-body base) (if (uri-query uri) (uri-query uri) (uri-query base)) (uri-fragment uri)))
	  ((equal? (string-ref (uri-part uri 'path) 0) #\/) 
		(make-uri 
			(uri-scheme base) 
			(string-append (if (uri-part base 'authority) (string-append "//" (uri-part base 'authority)) "") (remove-dot-segments (uri-part uri 'path))) 
			(uri-query uri) 
			(uri-fragment uri)))
	   ;otherwise  uri is relative to base so we need to merge the paths
	   (else  (make-uri (uri-scheme base) (string-append (or (uri-part base 'authority) "") (remove-dot-segments (merge-path base uri))) (uri-query uri) (uri-fragment uri)))))
	  
   (cond 
	((null? relatives) base)
	((null? (cdr relatives)) (resolve base (car relatives)))
	(else (map (lambda (uri) (resolve base uri)) relatives)))) 
       
	
;relative-uri creates a relative uri using the base-uri. It is the reverse of resolve-uri

(define (relative-uri base uri)
   (define (relativize-path base path)
       (define (last-index-of s c)
	    (let loop ((i (string-length s)))
		(if (< i 0) #f
		(if (char=? c (string-ref s i)) i (loop (- i 1))))))
	(let ((l1 (string-length path))
	      (l2 (string-length base)))
		(cond 
		   ((zero? l2) path)
		   ((char=? (string-ref path 0) #\/) path)
		   (else (string-append (substring base 0 (last-index-of base #\/)) path)))))
   (cond
	((not (uri? base)) (r6rs#assertion-violation 'relative-uri "first argument must be a uri" base))
	((not (uri? uri)) (r6rs#assertion-violation 'relative-uri "second argument must be a uri" uri))
	((not (eq? (uri-scheme base) (uri-scheme uri))) uri)
	((not (string=? (uri-part base 'authority) (uri-part uri 'authority))) uri)
	(else (make-uri (uri-scheme uri) (string-append "//" (uri-part uri 'authority) (relativize-path (uri-part base 'path) (uri-part uri 'path))) (uri-query uri) (uri-fragment uri)))))

;if the uri-part is not defined, then this will return #f. Otherwise, it will return a string.
;if the uri-part is not set, then it returns the empty string or #t.

(define uri-part #f)
(define register-uri-part! #f)

; returns #t if the uri has the part set and not empty, false otherwise
(define (uri-part? uri part)
	(let ((result (uri-part uri part)))
	   (and result (not (equal? result "")))))

(let ((part-registry (make-hash-table)))
   (set! register-uri-part!
	(lambda (scheme name fn)
	    (hash-table-set! part-registry (cons scheme name) fn)))
   (set! uri-part 
	(lambda (uri part)
	  ((hash-table-ref/default part-registry (cons (uri-scheme uri) part) (lambda rest #f)) (uri-body uri)))))

;register some parsers
(let ()
	;common parsers
  (define (uri-authority body)
    (cond 
    ((string=? (substring body 0 2) "//")
     (let ((rest (substring body 2 (string-length body))))
      (substring rest 0 (or (index-of rest #\/) (string-length rest)))))
    (else #f)))

  (define (uri-path body)
   (cond 
    ((string=? (substring body 0 (min (string-length body) 2)) "//")
     (let ((rest (substring body 2 (string-length body))))
      (and (index-of rest #\/) (substring rest (index-of rest #\/)  (string-length rest)))))
    (else body)))

  (define (index-of s c)
		(let loop ((i 0))
		   (cond 
			((= i (string-length s)) #f)
		   	((eq? (string-ref s i) c) i)
			(else (loop (+ i 1))))))
  (register-uri-part! #f 'path uri-path)
 (for-each (lambda (scheme)
    (register-uri-part! scheme 'path uri-path)
    (register-uri-part! scheme 'authority uri-authority))
    '(file http ftp shttp gopher nfs https tftp))  

)

